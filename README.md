# VoogPHP - PHP API for the VOOG CMS platform

VoogPHP is a PHP implementation of the [VOOG](https://www.voog.com) CMS platform API. 

The implementation is currently at version 2.x - a major overhaul and not backward-compatible with 1.x. Sorry about that.

Also, the documentation is not very helpful at the moment, but the code should be quite self-explanatory. And most answers to "what does this parameter mean?" can be found in the [VOOG API documentation](https://www.voog.com/developers/api).
 

## Installation

Installation is best done using Composer, by including the following in your composer.json file:

```
"require": {
  "codelab/voogphp": ">=2.0.0"
},
```

The requirements of the API are quite subtle - a PHP, version 5.5 or later; and the FlaskHTTP HTTP request library. Oh, and ofcourse, a Voog website to work on.


## Usage

#### Initializing a connection

To create a connection, initialize a VoogPHP instance:

```
$Voog=new \Codelab\VoogPHP\VoogPHP(
  'http://yoursite.voog.com',  // Your Voog site URL
  '1234567890abcdef'           // Your Voog API token
);
```

Additionally, if you want to debug everything that's going on, you can add a log file path as the third parameter.

#### Using the API

Once the VoogPHP instance is initialized, it will contain the following sub-objects:

* Site
* Users
* AdminUsers
* Languages
* Nodes
* Layouts
* Pages
* Articles
* Elements
* ElementDefinitions
* MediaSets
* Assets
* Texts
* Forms
* ContentPartials
* BuyButtons
* Store
* Products
* Carts
* Discounts
* Orders
* Invoices
* ShippingMethods
* PaymentGateways

Almost all of them correspond to the sections in the [VOOG API documentation](https://www.voog.com/developers/api) with the same name.

The exceptions to the rule are:

* Contents - available under the respective objects (pages, articles, elements, languages)
* Comments - available under Articles
* Tags - available under Articles
* Tickets - available under Forms
* Layout assets - available under Layouts

## Help and problems

If you have any questions about this API, you can raise an issue in BitBucket or write to Indrek Siitan <indrek.siitan@codelab.ee>.

If you have questions about Voog or the underlying Voog API, the best place to ask for advice is the Voog Slack channel at [slack.voog.com](https://slack.voog.com).