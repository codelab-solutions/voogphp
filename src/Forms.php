<?php


	/**
	 *
	 *   Voog PHP API
	 *   Form functions
	 *   --------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class Forms extends VoogPHPComponent
	{


		/**
		 *
		 *   Get forms
		 *   ---------
		 *   @access public
		 *   @param string $parent_type Parent type
		 *   @param int $parent_id Parent ID
		 *   @param array|object $data Additional filter data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getForms( $parent_type=null, $parent_id=null, $data=null )
		{
			// Build request
			$requestParam=array();
			if ($parent_type || $parent_id)
			{
				if (empty($parent_type)) throw new VoogParamException('Missing input data: $parent_type.');
				if (empty($parent_id)) throw new VoogParamException('Missing input data: $parent_id.');
				$requestParam['parent_type']=$parent_type;
				$requestParam['parent_id']=$parent_id;
			}
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestParam[$k]=$v;
				}
			}

			// Fetch
			$voogResponse=$this->VoogPHP->makeAPIrequest('/forms?'.http_build_query($requestParam));

			// Build response array
			$formsArray=array();
			foreach ($voogResponse as $form)
			{
				$formsArray[strval($form->id)]=$form;
			}

			// Return
			return $formsArray;
		}


		/**
		 *
		 *   Get form item
		 *   -------------
		 *   @access public
		 *   @param int $id Form ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getForm( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/forms/'.strval($id));
		}


		/**
		 *
		 *   Update form
		 *   -----------
		 *   @access public
		 *   @param int $id Form ID
		 *   @param array $data Form parameters
		 *   @param array $fields Form fields
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function updateForm( $id, $data=null, $fields=null )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');
			if (empty($data) && empty($fields)) throw new VoogParamException('Missing input data: $data or $fields.');

			// Create data
			$requestData=new \stdClass();
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestData->{$k}=$v;
				}
			}
			if (!empty($fields))
			{
				if (!is_array($fields)) throw new VoogParamException('Invalid input data: fields not an array.');
				$requestData->fields=array();
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/forms/'.strval($id),'PUT',$requestData);
		}


		/**
		 *
		 *   Delete form
		 *   -----------
		 *   @access public
		 *   @param int $id Form ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteForm( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Delete
			return $this->VoogPHP->makeAPIrequest('/forms/'.strval($id),'DELETE');
		}


		/**
		 *
		 *   Get form tickets
		 *   ----------------
		 *   @access public
		 *   @param int $form_id Form ID
		 *   @param array|object $data Filter data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getTickets( $form_id, $data=null )
		{
			// Check
			if (empty($form_id)) throw new VoogParamException('Missing input data: $form_id.');

			// Request with parameters
			$requestParam=array();
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestParam[$k]=$v;
				}
			}

			// Fetch
			$voogResponse=$this->VoogPHP->makeAPIrequest('/forms/'.strval($form_id).'/tickets?'.http_build_query($requestParam),'GET');

			// Build response array
			$ticketsArray=array();
			foreach ($voogResponse as $ticket)
			{
				$ticketsArray[strval($ticket->id)]=$ticket;
			}

			// Return
			return $ticketsArray;
		}


		/**
		 *
		 *   Get a specific ticket
		 *   ---------------------
		 *   @access public
		 *   @param int $form_id Form ID
		 *   @param int $ticket_id Ticket ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getTicket( $form_id, $ticket_id )
		{
			// Check
			if (empty($form_id)) throw new VoogParamException('Missing input data: $form_id.');
			if (empty($ticket_id)) throw new VoogParamException('Missing input data: $ticket_id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/forms/'.strval($form_id).'/tickets/'.strval($ticket_id));
		}


		/**
		 *
		 *   Delete form ticket
		 *   ------------------
		 *   @access public
		 *   @param int $form_id Form ID
		 *   @param int $ticket_id Ticket ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteTicket( $form_id, $ticket_id )
		{
			// Check
			if (empty($form_id)) throw new VoogParamException('Missing input data: $form_id.');
			if (empty($ticket_id)) throw new VoogParamException('Missing input data: $ticket_id.');

			// Delete
			return $this->VoogPHP->makeAPIrequest('/forms/'.strval($form_id).'/tickets/'.strval($ticket_id),'DELETE');
		}


		/**
		 *
		 *   Delete multiple tickets
		 *   -----------------------
		 *   @access public
		 *   @param int $form_id Form ID
		 *   @param array $ticket_ids Ticket IDs
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteTickets( $form_id, $ticket_ids )
		{
			// Check
			if (empty($form_id)) throw new VoogParamException('Missing input data: $form_id.');
			if (empty($ticket_ids)) throw new VoogParamException('Missing input data: $ticket_ids.');
			if (!is_array($ticket_ids)) throw new VoogParamException('Invalid input data: $ticket_ids not an array.');

			// Build request
			$requestData=new \stdClass();
			$requestData->ticket_ids=$ticket_ids;

			// Delete
			return $this->VoogPHP->makeAPIrequest('/forms/'.strval($form_id).'/tickets/delete_tickets','DELETE',$requestData);
		}


		/**
		 *
		 *   Delete all tickets marked as spam
		 *   ---------------------------------
		 *   @access public
		 *   @param int $form_id Form ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteSpam( $form_id )
		{
			// Check
			if (empty($form_id)) throw new VoogParamException('Missing input data: $form_id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/forms/'.strval($form_id).'/tickets/delete_spam','DELETE');
		}


	}


?>