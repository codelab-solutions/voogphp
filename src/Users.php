<?php


	/**
	 *
	 *   Voog PHP API
	 *   User functions
	 *   --------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class Users extends VoogPHPComponent
	{


		/**
		 *
		 *   Get users
		 *   ---------
		 *   @access public
		 *   @param array|object $data Filter data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getUsers( $data=null )
		{
			// Request with parameters
			$requestParam=array();
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestParam[$k]=$v;
				}
			}

			// Fetch list
			$voogResponse=$this->VoogPHP->makeAPIrequest('/site_users?'.http_build_query($requestParam));

			// Build response array
			$userArray=array();
			foreach ($voogResponse as $user)
			{
				$userArray[strval($user->id)]=$user;
			}

			// Return
			return $userArray;
		}


		/**
		 *
		 *   Get user info
		 *   -------------
		 *   @access public
		 *   @param int $id User ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getUser( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/site_users/'.strval($id));
		}


		/**
		 *
		 *   Create a new user
		 *   -----------------
		 *   @access public
		 *   @param string $email E-mail address
		 *   @param int $page_id Page ID to give access to
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function addUser( $email, $page_id=null )
		{
			// Check
			if (empty($email)) throw new VoogParamException('Missing input data: $email.');

			// Create data
			$requestData=new \stdClass();
			$requestData->email=$email;
			if ($page_id) $requestData->page_id=$page_id;

			// Make request
			return $this->VoogPHP->makeAPIrequest('/site_users','POST',$requestData);
		}


		/**
		 *
		 *   Remove user
		 *   -----------
		 *   @access public
		 *   @param int $id User ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteUser( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/site_users/'.strval($id),'DELETE');
		}


	}


?>