<?php


	/**
	 *
	 *   Voog PHP API
	 *   Order functions
	 *   ---------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class Orders extends VoogPHPComponent
	{


		/**
		 *
		 *   Get orders
		 *   ----------
		 *   @access public
		 *   @param string $languageCode Language code for top-level values
		 *   @param array|object $data Filter data
		 *   @param bool $includeItems Include item lines
		 *   @param bool $includeVariants Include variants
		 *   @param bool $includeTranslations Include translations
		 *   @param bool $includeShippingAddress Include shipping address
		 *   @param bool $includeBillingAddress Include billing address
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getOrders( $languageCode=null, $data=null, $includeItems=false, $includeVariants=false, $includeTranslations=false, $includeShippingAddress=false, $includeBillingAddress=false )
		{
			// Request
			$requestParam=array();
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v) $requestParam[$k]=$v;
			}
			if (!empty($languageCode)) $requestParam['language_code']=$languageCode;
			$include=array();
			if ($includeItems) $include[]='items';
			if ($includeVariants) $include[]='variants';
			if ($includeTranslations) $include[]='translations';
			if ($includeShippingAddress) $include[]='shipping_address';
			if ($includeBillingAddress) $include[]='billing_address';
			if (!empty($include))
			{
				$requestParam['include']=join(',',$include);
			}

			// Fetch list
			$voogResponse=$this->VoogPHP->makeAPIrequest('/ecommerce/v1/orders?'.http_build_query($requestParam));

			// Build response array
			$ordersArray=array();
			foreach ($voogResponse as $product)
			{
				$ordersArray[strval($product->id)]=$product;
			}

			// Return
			return $ordersArray;
		}


		/**
		 *
		 *   Get order data
		 *   --------------
		 *   @access public
		 *   @param int $id Order ID
		 *   @param bool $includeItems Include item lines
		 *   @param bool $includeVariants Include variants
		 *   @param bool $includeTranslations Include translations
		 *   @param bool $includeShippingAddress Include shipping address
		 *   @param bool $includeBillingAddress Include billing address
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getOrder( $id, $includeItems=false, $includeVariants=false, $includeTranslations=false, $includeShippingAddress=false, $includeBillingAddress=false )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Request
			$requestParam=array();
			$include=array();
			if ($includeItems) $include[]='items';
			if ($includeVariants) $include[]='variants';
			if ($includeTranslations) $include[]='translations';
			if ($includeShippingAddress) $include[]='shipping_address';
			if ($includeBillingAddress) $include[]='billing_address';
			if (!empty($include))
			{
				$requestParam['include']=join(',',$include);
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/orders/'.strval($id).'?'.http_build_query($requestParam));
		}


		/**
		 *
		 *   Update order data
		 *   -----------------
		 *   @access public
		 *   @param int $id Order ID
		 *   @param array|object $data Update data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function updateOrder( $id, $data )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');
			if (empty($data)) throw new VoogParamException('Missing input data: $data.');
			if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');

			// Create data
			$requestData=new \stdClass();
			foreach ($data as $k => $v)
			{
				$requestData->{$k}=$v;
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/orders/'.strval($id),'PUT',$requestData);
		}


	}


?>