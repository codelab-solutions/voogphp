<?php


	/**
	 *
	 *   Voog PHP API
	 *   Buy button functions
	 *   --------------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class BuyButtons extends VoogPHPComponent
	{


		/**
		 *
		 *   Get buy buttons
		 *   ---------------
		 *   @access public
		 *   @param int $page_id Page ID
		 *   @param array|object $data Additional filter data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getBuyButtons( $page_id=null, $data=null )
		{
			// Build request
			$requestParam=array();
			if (!empty($page_id))
			{
				$requestParam['q.content.parent_id']=strval($page_id);
			}
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestParam[$k]=$v;
				}
			}

			// Fetch
			$voogResponse=$this->VoogPHP->makeAPIrequest('/buy_buttons?'.http_build_query($requestParam));

			// Build response array
			$buyButtonArray=array();
			foreach ($voogResponse as $buyButton)
			{
				$buyButtonArray[strval($buyButton->id)]=$buyButton;
			}

			// Return
			return $buyButtonArray;
		}


		/**
		 *
		 *   Get a single buy button info
		 *   ----------------------------
		 *   @access public
		 *   @param int $id Buy button ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getBuyButton( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/buy_buttons/'.strval($id));
		}


		/**
		 *
		 *   Update a buy button
		 *   -------------------
		 *   @access public
		 *   @param int $id Buy button ID
		 *   @param array|object $data Update data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function updateBuyButton( $id, $data )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');
			if (empty($data)) throw new VoogParamException('Missing input data: $data.');
			if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');

			// Create data
			$requestData=new \stdClass();
			foreach ($data as $k => $v)
			{
				$requestData->{$k}=$v;
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/buy_buttons/'.strval($id),'PUT',$requestData);
		}


	}


?>