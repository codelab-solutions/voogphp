<?php


	/**
	 *
	 *   Voog PHP API
	 *   Page functions
	 *   --------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class Pages extends VoogPHPComponent
	{


		/**
		 *
		 *   Get pages
		 *   ---------
		 *   @access public
		 *   @param array|object $data Filter data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getPages( $data=null )
		{
			// Request parameters
			$requestParam=array();
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestParam[$k]=$v;
				}
			}

			// Fetch
				$voogResponse=$this->VoogPHP->makeAPIrequest('/pages?'.http_build_query($requestParam));

			// Build response array
			$pageArray=array();
			foreach ($voogResponse as $page)
			{
				$pageArray[strval($page->id)]=$page;
			}

			// Return
			return $pageArray;
		}


		/**
		 *
		 *   Get page
		 *   --------
		 *   @access public
		 *   @param int $id Page ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getPage( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/pages/'.strval($id));
		}


		/**
		 *
		 *   Get page by URL
		 *   ---------------
		 *   @access public
		 *   @param int $url Page URL
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 */

		public function getPageByURL( $url )
		{
			// Check
			if (empty($url)) throw new VoogParamException('Missing input data: $url.');

			// Remove leading slash
			if (!strncmp($url,'/',1))
			{
				$url=mb_substr($url,1);
			}

			// Get list
			$pageList=$this->getPages(array('path'=>$url));
			if (!sizeof($pageList)) throw new VoogParamException('Page /'.$url.' not found.');
			if (sizeof($pageList)>1) throw new VoogParamException('Multiple matches for /'.$url.' found.');

			// Return
			foreach ($pageList as $page)
			{
				return $this->getPage(strval($page->id));
			}
		}


		/**
		 *
		 *   Create a page
		 *   -------------
		 *   @access public
		 *   @param string $title Page title
		 *   @param string $slug URL slug
		 *   @param int $parent_id Parent ID
		 *   @param int $layout_id Layout ID
		 *   @param array $data Additional page data
		 *   @param array $pageData Additional page custom data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function addPage( $title, $slug, $parent_id, $layout_id, $data=null, $pageData=null )
		{
			// Check
			if (empty($title)) throw new VoogParamException('Missing input data: $title.');
			if (empty($slug)) throw new VoogParamException('Missing input data: $slug.');
			if (empty($parent_id)) throw new VoogParamException('Missing input data: $parent_id.');
			if (empty($layout_id)) throw new VoogParamException('Missing input data: $layout_id.');

			// Create data
			$requestData=new \stdClass();
			$requestData->title=$title;
			$requestData->slug=$slug;
			$requestData->layout_id=$layout_id;
			$requestData->parent_id=$parent_id;
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestData->{$k}=$v;
				}
			}
			if (!empty($pageData))
			{
				$requestData->data=new \stdClass();
				foreach ($pageData as $k => $v)
				{
					$requestData->data->{$k}=$v;
				}
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/pages','POST',$requestData);
		}


		/**
		 *
		 *   Update a page
		 *   -------------
		 *   @access public
		 *   @param int $id Page ID
		 *   @param array|object $data Additional page data
		 *   @param array|object $pageData Additional page custom data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function updatePage( $id, $data=null, $pageData=null )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Create data
			$requestData=new \stdClass();
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestData->{$k}=$v;
				}
			}
			if (!empty($pageData))
			{
				if (!is_array($pageData) && !is_object($pageData)) throw new VoogParamException('Invalid input data: $pageData not an array or an object.');
				$requestData->data=new \stdClass();
				foreach ($pageData as $k => $v)
				{
					$requestData->data->{$k}=$v;
				}
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/pages/'.strval($id),'PATCH',$requestData);
		}


		/**
		 *
		 *   Delete page
		 *   -----------
		 *   @access public
		 *   @param int $id Page ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deletePage( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Delete
			return $this->VoogPHP->makeAPIrequest('/pages/'.strval($id),'DELETE');
		}


		/**
		 *
		 *   Set page custom data parameter
		 *   ------------------------------
		 *   @access public
		 *   @param int $page_id Page ID
		 *   @param string $paramName Parameter name
		 *   @param mixed $paramValue Parameter value
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function setPageCustomData( $page_id, $paramName, $paramValue )
		{
			// Check
			if (empty($page_id)) throw new VoogParamException('Missing input data: $page_id.');
			if (empty($paramName)) throw new VoogParamException('Missing input data: $paramName.');

			// Request
			$requestData=array(
				'value' => $paramValue
			);

			// Make request
			return $this->VoogPHP->makeAPIrequest('/pages/'.strval($page_id).'/data/'.$paramName,'PUT',$requestData);
		}


		/**
		 *
		 *   Remove page custom data parameter
		 *   ---------------------------------
		 *   @access public
		 *   @param int $page_id Page ID
		 *   @param string $paramName Parameter name
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteArticleCustomData( $page_id, $paramName )
		{
			// Check
			if (empty($page_id)) throw new VoogParamException('Missing input data: $page_id.');
			if (empty($paramName)) throw new VoogParamException('Missing input data: $paramName.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/articles/'.strval($page_id).'/data/'.$paramName,'DELETE');
		}


		/**
		 *
		 *   Get page contents
		 *   -----------------
		 *   @access public
		 *   @param int $page_id Page ID
		 *   @param array $data Additional request data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 */

		public function getPageContents( $page_id, $data=null )
		{
			// Check
			if (empty($page_id)) throw new VoogParamException('Missing input data: $page_id.');

			// Request parameters
			$requestParam=array();
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestParam[$k]=$v;
				}
			}

			// Fetch
			$voogResponse=$this->VoogPHP->makeAPIrequest('/pages/'.strval($page_id).'/contents?'.http_build_query($requestParam),'GET');

			// Build response array
			$contentsArray=array();
			foreach ($voogResponse as $content)
			{
				$contentsArray[strval($content->id)]=$content;
			}

			// Return
			return $contentsArray;
		}


		/**
		 *
		 *   Get page content item
		 *   ---------------------
		 *   @access public
		 *   @param int $page_id Page ID
		 *   @param int $content_id Content ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getPageContent( $page_id, $content_id )
		{
			// Check
			if (empty($page_id)) throw new VoogParamException('Missing input data: $page_id.');
			if (empty($content_id)) throw new VoogParamException('Missing input data: $content_id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/pages/'.strval($page_id).'/contents/'.strval($content_id));
		}


		/**
		 *
		 *   Create a page
		 *   -------------
		 *   @access public
		 *   @param int $page_id Page ID
		 *   @param array $data Content data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function addPageContent( $page_id, $data=null )
		{
			// Check
			if (empty($page_id)) throw new VoogParamException('Missing input data: $page_id.');
			if (empty($data)) throw new VoogParamException('Missing input data: $data.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/pages/'.strval($page_id).'/contents','POST',$data);
		}


		/**
		 *
		 *   Delete page
		 *   -----------
		 *   @access public
		 *   @param int $page_id Page ID
		 *   @param int $content_id Content ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deletePageContent( $page_id, $content_id )
		{
			// Check
			if (empty($page_id)) throw new VoogParamException('Missing input data: $page_id.');
			if (empty($content_id)) throw new VoogParamException('Missing input data: $content_id.');

			// Delete
			return $this->VoogPHP->makeAPIrequest('/pages/'.strval($page_id).'/contents/'.strval($content_id),'DELETE');
		}


	}


?>