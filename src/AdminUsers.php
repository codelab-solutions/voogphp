<?php


	/**
	 *
	 *   Voog PHP API
	 *   Admin user functions
	 *   --------------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class AdminUsers extends VoogPHPComponent
	{


		/**
		 *
		 *   Get admin users
		 *   ---------------
		 *   @access public
		 *   @param array|object $data Filter data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getAdminUsers( $data=null )
		{
			// Request with parameters
			$requestParam=array();
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestParam[$k]=$v;
				}
			}

			// Fetch list
			$voogResponse=$this->VoogPHP->makeAPIrequest('/people?'.http_build_query($requestParam));

			// Build response array
			$userArray=array();
			foreach ($voogResponse as $user)
			{
				$userArray[strval($user->id)]=$user;
			}

			// Return
			return $userArray;
		}


		/**
		 *   Get admin user info
		 *   @access public
		 *   @param int $id User ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 */

		public function getAdminUser( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/people/'.strval($id));
		}


	}


?>