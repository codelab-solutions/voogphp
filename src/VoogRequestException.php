<?php


	/**
	 *
	 *   Voog PHP API
	 *   The Voog request exception
	 *   --------------------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class VoogRequestException extends VoogException
	{
	}


?>