<?php


	/**
	 *
	 *   Voog PHP API
	 *   API component base class
	 *   ------------------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class VoogPHPComponent
	{


		/**
		 *   Backreference to the main object
		 *   @var VoogPHP
		 */

		public $VoogPHP = null;


		/**
		 *
		 *   Constructor
		 *   -----------
		 *   @access public
		 *   @param VoogPHP $VoogPHP Backreference to the main object
		 *   @return VoogPHPComponent
		 *
		 */

		public function __construct( $VoogPHP )
		{
			$this->VoogPHP=$VoogPHP;
		}


	}


?>