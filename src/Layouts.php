<?php


	/**
	 *
	 *   Voog PHP API
	 *   Layout functions
	 *   ----------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class Layouts extends VoogPHPComponent
	{


		/**
		 *
		 *   Get layouts
		 *   -----------
		 *   @access public
		 *   @param string $type Layout type
		 *   @param array $data Request data
		 *   @param bool $listOnly Return displayable list only
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getLayouts( $type=null, $data=null, $listOnly=false )
		{
			// Request parameters
			$requestParam=array();
			if (!empty($type))
			{
				$requestParam['content_type']=$type;
			}
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestParam[$k]=$v;
				}
			}

			// Fetch
			$voogResponse=$this->VoogPHP->makeAPIrequest('/layouts?'.http_build_query($requestParam));

			// Build response array
			$layoutArray=array();
			foreach ($voogResponse as $layout)
			{
				$layoutArray[strval($layout->id)]=($listOnly?strval($layout->title):$layout);
			}

			// Return
			return $layoutArray;
		}


		/**
		 *
		 *   Get layout
		 *   ----------
		 *   @access public
		 *   @param int $id Layout ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getLayout( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/layouts/'.strval($id));
		}


		/**
		 *
		 *   Get layout by name
		 *   ------------------
		 *   @access public
		 *   @param string $name Layout name
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getLayoutByName( $name )
		{
			// Check
			if (empty($name)) throw new VoogParamException('Missing input data: $name.');

			// Get list
			$layoutList=$this->getLayouts(null,array('q.layout.title.$eq'=>$name),false);
			if (!sizeof($layoutList)) throw new VoogRequestException('Layout '.$name.' not found.');
			if (sizeof($layoutList)>1) throw new VoogRequestException('Multiple matches for '.$name.' found.');

			// Return
			foreach ($layoutList as $layout)
			{
				return $this->getLayout(strval($layout->id));
			}
		}


		/**
		 *
		 *   Add a new layout
		 *   ----------------
		 *   @access public
		 *   @param string $title Layout title
		 *   @param string $type Layout type
		 *   @param string $body Layout content/body
		 *   @param string $parent_id Parent ID (for article/element layouts)
		 *   @param object|array $data Layout data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function addLayout( $title, $type, $body, $parent_id=null, $data=null )
		{
			// Check
			if (empty($title)) throw new VoogParamException('Missing input data: $title.');
			if (empty($type)) throw new VoogParamException('Missing input data: $type.');

			// Create data
			$requestData=new \stdClass();
			$requestData->title=$title;
			if ($type=='component')
			{
				$requestData->component=true;
			}
			else
			{
				$requestData->content_type=$type;
			}
			$requestData->body=$body;
			if (!empty($parent_id)) $requestData->parent_id=$parent_id;
			if (!empty($data))
			{
				foreach ($data as $k => $v)
				{
					$requestData->{$k}=$v;
				}
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/layouts','POST',$requestData);
		}


		/**
		 *
		 *   Update layout
		 *   -------------
		 *   @access public
		 *   @param int $id Layout ID
		 *   @param string $body Layout content/body
		 *   @param object|array $data Layout data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function updateLayout( $id, $body=null, $data=null )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');
			if ($body===null && empty($data)) throw new VoogParamException('Missing input data: $body or $data.');

			// Create data
			$requestData=new \stdClass();
			if ($body!==null)
			{
				$requestData->body=$body;
			}
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestData->{$k}=$v;
				}
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/layouts/'.strval($id),'PUT',$requestData);
		}


		/**
		 *
		 *   Delete a layout
		 *   ---------------
		 *   @access public
		 *   @param int $id Layout ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteLayout( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Delete
			return $this->VoogPHP->makeAPIrequest('/layouts/'.strval($id),'DELETE');
		}


		/**
		 *
		 *   Get layout assets
		 *   -----------------
		 *   @access public
		 *   @param string $type Asset type
		 *   @param array $data Additional request/filter data
		 *   @param bool $listOnly Return displayable list only
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getLayoutAssets( $type=null, $data=null, $listOnly=false )
		{
			// Request parameters
			$requestParam=array();
			if (!empty($type))
			{
				$requestParam['q.layout_asset.asset_type.$eq']=$type;
			}
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestParam[$k]=$v;
				}
			}

			// Fetch
			$voogResponse=$this->VoogPHP->makeAPIrequest('/layout_assets?'.http_build_query($requestParam));

			// Build response array
			$layoutAssetArray=array();
			foreach ($voogResponse as $asset)
			{
				$layoutAssetArray[strval($asset->id)]=($listOnly?strval($asset->filename):$asset);
			}

			// Return
			return $layoutAssetArray;
		}


		/**
		 *
		 *   Get layout asset
		 *   ----------------
		 *   @access public
		 *   @param int $id Layout asset ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getLayoutAsset( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/layout_assets/'.strval($id));
		}


		/**
		 *
		 *   Get layout asset by filename
		 *   ----------------------------
		 *   @access public
		 *   @param string $filename Filename
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getLayoutAssetByFilename( $filename )
		{
			// Check
			if (empty($filename)) throw new VoogParamException('Missing input data: $filename.');

			// Get list
			$layoutAssetList=$this->getLayoutAssets(null,array('q.layout_asset.filename.$eq'=>$filename),false);
			if (!sizeof($layoutAssetList)) throw new VoogRequestException('Layout asset '.$filename.' not found.');
			if (sizeof($layoutAssetList)>1) throw new VoogRequestException('Multiple matches for '.$filename.' found.');

			// Return
			foreach ($layoutAssetList as $layoutAsset)
			{
				return $this->getLayoutAsset(strval($layoutAsset->id));
			}
		}


		/**
		 *
		 *   Add a new layout asset
		 *   ----------------------
		 *   @access public
		 *   @param string $filename File name
		 *   @param string $body Content as string
		 *   @param string $sourceFilename Source filename
		 *   @param string $contentType Content type
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function addLayoutAsset( $filename, $body=null, $sourceFilename=null, $contentType=null )
		{
			// Check
			if (empty($filename)) throw new VoogParamException('Missing input data: $filename.');
			if (empty($sourceFilename))
			{
				if ($body===null) throw new VoogParamException('Missing input data: $body or $sourceFilename.');
			}
			if ($body!==null && !empty($sourceFilename)) throw new VoogParamException('Invalid input data: either $body or $sourceFilename can be supplied, but not both.');

			// Create data
			$requestData=new \stdClass();
			$requestData->filename=$filename;
			if ($body!==null) $requestData->data=$body;
			if (!empty($contentType)) $requestData->content_type=$contentType;

			// File upload
			$fileUploads=null;
			if (!empty($sourceFilename))
			{
				$fileUploads=array();
				$fileUploads['file']=$sourceFilename;
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/layout_assets','POST',$requestData,null,$fileUploads);
		}


		/**
		 *
		 *   Update a layout asset
		 *   ---------------------
		 *   @access public
		 *   @param int $id Layout asset ID
		 *   @param string $body Content as string
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function updateLayoutAsset( $id, $body )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $filename.');
			if ($body===null) throw new VoogParamException('Missing input data: $body.');

			// Create data
			$requestData=new \stdClass();
			if (!empty($body)) $requestData->data=$body;

			// Make request
			return $this->VoogPHP->makeAPIrequest('/layout_assets/'.strval($id),'PUT',$requestData);
		}


		/**
		 *
		 *   Delete a layout asset
		 *   ---------------------
		 *   @access public
		 *   @param int $id Layout asset ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteLayoutAsset( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Delete
			return $this->VoogPHP->makeAPIrequest('/layout_assets/'.strval($id),'DELETE');
		}


	}


?>