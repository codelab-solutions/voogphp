<?php


	/**
	 *
	 *   Voog PHP API
	 *   Payment gateway functions
	 *   -------------------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class PaymentGateways extends VoogPHPComponent
	{


		/**
		 *
		 *   Get payment gateways
		 *   --------------------
		 *   @access public
		 *   @param bool $includeGatewaySettings Include gateway settings
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getPaymentGateways( $includeGatewaySettings=false )
		{
			// Request
			$requestParam=array();
			if ($includeGatewaySettings) $requestParam['gateway_settings']='true';

			// Return list
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/gateways?'.http_build_query($requestParam));
		}


		/**
		 *
		 *   Get payment gateway info
		 *   ------------------------
		 *   @access public
		 *   @param string $code Gateway code
		 *   @param bool $includeGatewaySettings Include gateway settings
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getPaymentGateway( $code, $includeGatewaySettings=true )
		{
			// Check
			if (empty($code)) throw new VoogParamException('Missing input data: $code.');

			// Request
			$requestParam=array();
			if ($includeGatewaySettings) $requestParam['gateway_settings']='true';

			// Make request
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/gateways/'.strval($code).'?'.http_build_query($requestParam));
		}


		/**
		 *
		 *   Update payment gateway attributes
		 *   ---------------------------------
		 *   @access public
		 *   @param string $code Gateway code
		 *   @param array $data Gateway attribtues
		 *   @param array $settings Gateway-specific settings
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function updatePaymentGateway( $code, $data=null, $settings=null )
		{
			// Check
			if (empty($code)) throw new VoogParamException('Missing input data: $code.');
			if (empty($data) && empty($settings)) throw new VoogParamException('Missing input data: $data or $settings.');

			// Create data
			$requestData=new \stdClass();
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or object.');
				foreach ($data as $k => $v)
				{
					$requestData->{$k}=$v;
				}
			}
			if (!empty($settings))
			{
				if (!is_array($settings) && !is_object($settings)) throw new VoogParamException('Invalid input data: $settings not an array or object.');
				foreach ($settings as $k => $v)
				{
					$requestData->{$k}=$v;
				}
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/gateways/'.strval($code),'PUT',$requestData);
		}


		/**
		 *
		 *   Delete a payment gateway
		 *   ------------------------
		 *   @access public
		 *   @param string $code Gateway code
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deletePaymentGateway( $code )
		{
			// Check
			if (empty($code)) throw new VoogParamException('Missing input data: code.');

			// Delete
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/gateways/'.strval($code),'DELETE');
		}


	}


?>