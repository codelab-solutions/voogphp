<?php


	/**
	 *
	 *   Voog PHP API
	 *   Element functions
	 *   -----------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class Elements extends VoogPHPComponent
	{


		/**
		 *   Get element list
		 *   ----------------
		 *   @access public
		 *   @param int $element_definition_id Element definition ID
		 *   @param int|string $lang Element (ID or string)
		 *   @param int|string $page Page (ID or path)
		 *   @param array $params Additional parameters
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getElements( $element_definition_id, $lang=null, $page=null, $params=null )
		{
			// Check
			if (empty($element_definition_id)) throw new VoogParamException('Missing input data: $element_definition_id.');

			// Build query
			$requestParam=array();
			$requestParam['element_definition_id']=$element_definition_id;
			$requestParam['include_values']='true';
			if (!empty($lang))
			{
				if (is_numeric($lang))
				{
					$requestParam['language_id']=$lang;
				}
				else
				{
					$requestParam['language_code']=$lang;
				}
			}
			if (!empty($page))
			{
				if (is_numeric($page))
				{
					$requestParam['page_id']=$page;
				}
				else
				{
					$requestParam['page_path']=$page;
				}
			}
			if (!empty($params) && is_array($params))
			{
				foreach ($params as $k => $v)
				{
					$requestParam[$k]=$v;
				}
			}

			// Make request
			$voogResponse=$this->VoogPHP->makeAPIrequest('/elements?'.http_build_query($requestParam));

			// Build response array
			$elementArray=array();
			foreach ($voogResponse as $element)
			{
				$elementArray[strval($element->id)]=$element;
			}

			// Return
			return $elementArray;
		}


		/**
		 *
		 *   Get element
		 *   -----------
		 *   @access public
		 *   @param int $id Element ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getElement( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/elements/'.strval($id));
		}


		/**
		 *
		 *   Create element
		 *   --------------
		 *   @access public
		 *   @param int $element_definition_id Element definition ID
		 *   @param int $page_id Page ID
		 *   @param string $title
		 *   @param array|object $data Element values/data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function addElement( $element_definition_id, $page_id, $title, $data=null )
		{
			// Check
			if (empty($element_definition_id)) throw new VoogParamException('Missing input data: $element_definition_id.');
			if (empty($page_id)) throw new VoogParamException('Missing input data: $page_id.');
			if (empty($title)) throw new VoogParamException('Missing input data: $title.');

			// Create data
			$requestData=new \stdClass();
			$requestData->element_definition_id=intval($element_definition_id);
			$requestData->page_id=intval($page_id);
			$requestData->title=strval($title);
			$requestData->values=new \stdClass();
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestData->values->{$k}=$v;
				}
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/elements','POST',$requestData);
		}


		/**
		 *
		 *   Update element
		 *   --------------
		 *   @access public
		 *   @param int $id Element ID
		 *   @param array|object $data Element values/data
		 *   @param string $title Title (if updated)
		 *   @param string $path Path (if updated)
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function updateElement( $id, $data, $title=null, $path=null )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');
			if (empty($data)) throw new VoogParamException('Missing input data: $data.');
			if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');

			// Create data
			$requestData=new \stdClass();
			if (!empty($title))
			{
				$requestData->title=$title;
			}
			if (!empty($path))
			{
				$requestData->path=$path;
			}

			// Values
			$requestData->values=new \stdClass();
			foreach ($data as $k => $v)
			{
				$requestData->values->{$k}=$v;
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/elements/'.strval($id),'PUT',$requestData);
		}


		/**
		 *
		 *   Move element before another
		 *   ---------------------------
		 *   @access public
		 *   @param int $id Element ID
		 *   @param int $before Element ID, before what to move
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function moveElementBefore( $id, $before )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');
			if (empty($before)) throw new VoogParamException('Missing input data: $before.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/elements/'.strval($id).'/move?before='.intval($before),'PUT');
		}


		/**
		 *
		 *   Move element after another
		 *   --------------------------
		 *   @access public
		 *   @param int $id Element ID
		 *   @param int $after Element ID, after what to move
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function moveElementAfter( $id, $after )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');
			if (empty($after)) throw new VoogParamException('Missing input data: $after.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/elements/'.strval($id).'/move?after='.intval($after),'PUT');
		}


		/**
		 *
		 *   Delete element
		 *   --------------
		 *   @access public
		 *   @param int $id Element ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteElement( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Delete
			return $this->VoogPHP->makeAPIrequest('/elements/'.strval($id),'DELETE');
		}


		/**
		 *
		 *   Get element contents
		 *   --------------------
		 *   @access public
		 *   @param int $element_id Element ID
		 *   @param array $data Additional request data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getElementContents( $element_id, $data=null )
		{
			// Check
			if (empty($element_id)) throw new VoogParamException('Missing input data: $element_id.');

			// Request parameters
			$requestParam=array();
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestParam[$k]=$v;
				}
			}

			// Fetch
			$voogResponse=$this->VoogPHP->makeAPIrequest('/elements/'.strval($element_id).'/contents?'.http_build_query($requestParam),'GET');

			// Build response array
			$contentsArray=array();
			foreach ($voogResponse as $content)
			{
				$contentsArray[strval($content->id)]=$content;
			}

			// Return
			return $contentsArray;
		}


		/**
		 *
		 *   Get element content
		 *   -------------------
		 *   @access public
		 *   @param int $element_id Element ID
		 *   @param int $content_id Content ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getElementContent( $element_id, $content_id )
		{
			// Check
			if (empty($element_id)) throw new VoogParamException('Missing input data: $element_id.');
			if (empty($content_id)) throw new VoogParamException('Missing input data: $content_id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/elements/'.strval($element_id).'/contents/'.strval($content_id));
		}


		/**
		 *
		 *   Create an element content item
		 *   ------------------------------
		 *   @access public
		 *   @param int $element_id Element ID
		 *   @param array $data Content data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function addElementContent( $element_id, $data=null )
		{
			// Check
			if (empty($element_id)) throw new VoogParamException('Missing input data: $element_id.');
			if (empty($data)) throw new VoogParamException('Missing input data: $data.');
			if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/elements/'.strval($element_id).'/contents','POST',$data);
		}


		/**
		 *
		 *   Delete element content item
		 *   ---------------------------
		 *   @access public
		 *   @param int $element_id Element ID
		 *   @param int $content_id Content ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteElementContent( $element_id, $content_id )
		{
			// Check
			if (empty($element_id)) throw new VoogParamException('Missing input data: $element_id.');
			if (empty($content_id)) throw new VoogParamException('Missing input data: $content_id.');

			// Delete
			return $this->VoogPHP->makeAPIrequest('/elements/'.strval($element_id).'/contents/'.strval($content_id),'DELETE');
		}


	}


?>