<?php


	/**
	 *
	 *   Voog PHP API
	 *   E-commerce discount functions
	 *   -----------------------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class Discounts extends VoogPHPComponent
	{


		/**
		 *
		 *   Get discounts
		 *   -------------
		 *   @access public
		 *   @param bool $includeProducts Include item lines
		 *   @param bool $includeOrders Include variants
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getDiscounts( $includeProducts=false, $includeOrders=false )
		{
			// Request
			$requestParam=array();
			$include=array();
			if ($includeProducts) $include[]='discount_objects';
			if ($includeOrders) $include[]='orders';
			if (!empty($include))
			{
				$requestParam['include']=join(',',$include);
			}

			// Fetch list
			$voogResponse=$this->VoogPHP->makeAPIrequest('/ecommerce/v1/discounts?'.http_build_query($requestParam));

			// Build response array
			$discountsArray=array();
			foreach ($voogResponse as $product)
			{
				$discountsArray[strval($product->id)]=$product;
			}

			// Return
			return $discountsArray;
		}


		/**
		 *
		 *   Get discount info
		 *   -----------------
		 *   @access public
		 *   @param int $id Discount ID
		 *   @param bool $includeProducts Include item lines
		 *   @param bool $includeOrders Include variants
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getDiscount( $id, $includeProducts=false, $includeOrders=false )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Request
			$requestParam=array();
			$include=array();
			if ($includeProducts) $include[]='discount_objects';
			if ($includeOrders) $include[]='orders';
			if (!empty($include))
			{
				$requestParam['include']=join(',',$include);
			}

			// Fetch list
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/discounts/'.strval($id).'?'.http_build_query($requestParam));
		}


		/**
		 *
		 *   Add a new discount
		 *   ------------------
		 *   @access public
		 *   @param string $code Discount code
		 *   @param array $data Additional discount data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function addDiscount( $code, $data=null )
		{
			// Check
			if (empty($code)) throw new VoogParamException('Missing input data: $code.');

			// Create data
			$requestData=new \stdClass();
			$requestData->code=$code;
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestData->{$k}=$v;
				}
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/discounts','POST',$requestData);
		}


		/**
		 *
		 *   Update discount info
		 *   --------------------
		 *   @access public
		 *   @param int $id Discount ID
		 *   @param array $data Additional discount data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function updateDiscount( $id, $data=null )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Create data
			$requestData=new \stdClass();
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestData->{$k}=$v;
				}
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/discounts/'.strval($id),'PUT',$requestData);
		}


		/**
		 *
		 *   Delete a discount
		 *   -----------------
		 *   @access public
		 *   @param int $id Discount ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteDiscount( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Delete
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/discounts/'.strval($id),'DELETE');
		}


	}


?>