<?php


	/**
	 *
	 *   Voog PHP API
	 *   General e-store functions
	 *   -------------------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class Store extends VoogPHPComponent
	{


		/**
		 *
		 *   Get store settings
		 *   ------------------
		 *   @access public
		 *   @param string $languageCode Language to return the translatable values in
		 *   @param bool $includeTranslations Include relevant translations
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getStoreSettings( $languageCode=null, $includeTranslations=false )
		{
			// Request
			$requestParam=array();
			if ($languageCode) $requestParam['language_code']=$languageCode;
			if ($includeTranslations) $requestParam['include']='translations';
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/settings?'.http_build_query($requestParam));
		}


		/**
		 *
		 *   Update store settings
		 *   ---------------------
		 *   @access public
		 *   @param array|object $data Update data
		 *   @param array|object $translations Translations
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function updateStoreSettings( $data, $translations=null )
		{
			// Check
			if (empty($data)) throw new VoogParamException('Missing input data: $data.');
			if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');

			// Create data
			$requestData=new \stdClass();
			foreach ($data as $k => $v)
			{
				$requestData->{$k}=$v;
			}

			// Translations
			if (!empty($translations))
			{
				if (!is_array($translations) && !is_object($translations)) throw new VoogParamException('Invalid input data: $translations not an array or an object.');
				$requestData->translations=new \stdClass();
				foreach ($translations as $k => $translation)
				{
					if (!is_array($translation) && !is_object($translation)) throw new VoogParamException('Invalid input data: $translations['.$k.'] not an array or an object.');
					$requestData->translations->{$k}=new \stdClass();
					foreach ($translation as $lang => $value)
					{
						$requestData->translations->{$k}->{$lang}=$value;
					}
				}
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/settings','PUT',$requestData);
		}


	}


?>