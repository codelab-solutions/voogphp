<?php


	/**
	 *
	 *   Voog PHP API
	 *   The Voog parameter exception
	 *   ----------------------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class VoogParamException extends VoogException
	{
	}


?>