<?php


	/**
	 *
	 *   Voog PHP API
	 *   Menu node functions
	 *   -------------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class Nodes extends VoogPHPComponent
	{


		/**
		 *
		 *   Get nodes
		 *   ---------
		 *   @access public
		 *   @param int $parent_id Parent node ID
		 *   @param array $data Additional request/filter data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getNodes( $parent_id=null, $data=null )
		{
			// Request with parameters
			$requestParam=array();
			if (!empty($parent_id))
			{
				$requestParam['parent_id']=$parent_id;
			}
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestParam[$k]=$v;
				}
			}

			// Fetch list
			$voogResponse=$this->VoogPHP->makeAPIrequest('/nodes?'.http_build_query($requestParam));

			// Build response array
			$nodeArray=array();
			foreach ($voogResponse as $node)
			{
				$nodeArray[strval($node->id)]=$node;
			}

			// Return
			return $nodeArray;
		}


		/**
		 *
		 *   Get a specific node
		 *   -------------------
		 *   @access public
		 *   @param int $id Node ID
		 *   @param bool $includeChildren Include children in response
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getNode( $id, $includeChildren=false )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Request parameters
			$requestParam=array();
			if ($includeChildren)
			{
				$requestParam['incude_children']='true';
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/nodes/'.strval($id).'?'.http_build_query($requestParam));
		}


		/**
		 *
		 *   Update node attributes
		 *   ----------------------
		 *   @access public
		 *   @param int $id Language ID
		 *   @param array $data Node data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function updateNode( $id, $data )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');
			if (empty($data)) throw new VoogParamException('Missing input data: $data.');
			if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');

			// Create data
			$requestData=new \stdClass();
			foreach ($data as $k => $v)
			{
				$requestData->{$k}=$v;
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/nodes/'.strval($id),'PUT',$requestData);
		}


		/**
		 *
		 *   Move node
		 *   ---------
		 *   @access public
		 *   @param int $node_id Node ID
		 *   @param int $parent_id Parent ID
		 *   @param int $position Position
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function moveNode( $node_id, $parent_id, $position=null )
		{
			// Check
			if (empty($node_id)) throw new VoogParamException('Missing input data: node_id.');
			if (empty($parent_id)) throw new VoogParamException('Missing input data: parent_id.');

			// Make request
			$requestParam=array();
			$requestParam['parent_id']=$parent_id;
			if (empty($position)) $requestParam['position']=$position;
			return $this->VoogPHP->makeAPIrequest('/nodes/'.strval($node_id).'/move?'.http_build_query($requestParam),'PUT');
		}


	}


?>