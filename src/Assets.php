<?php


	/**
	 *
	 *   Voog PHP API
	 *   Asset functions
	 *   ---------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;
	use Codelab\FlaskHTTP\HttpRequest;


	class Assets extends VoogPHPComponent
	{


		/**
		 *
		 *   Get assets
		 *   ----------
		 *   @access public
		 *   @param array|object $data Filter data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getAssets( $data=null )
		{
			// Request parameters
			$requestParam=array();
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestParam[$k]=$v;
				}
			}

			// Fetch list
			$voogResponse=$this->VoogPHP->makeAPIrequest('/assets?'.http_build_query($requestParam));

			// Build response array
			$assetArray=array();
			foreach ($voogResponse as $asset)
			{
				$assetArray[strval($asset->id)]=$asset;
			}

			// Return
			return $assetArray;
		}


		/**
		 *
		 *   Get asset info
		 *   --------------
		 *   @access public
		 *   @param int $id Asset ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getAsset( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/assets/'.strval($id));
		}


		/**
		 *
		 *   Get asset by filename
		 *   ---------------------
		 *   @access public
		 *   @param string $filename File name
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getAssetByFilename( $filename )
		{
			// Check
			if (empty($filename)) throw new VoogParamException('Missing input data: $filename.');

			// Get list
			$assetList=$this->getAssets(array('name'=>$filename));
			if (!sizeof($assetList)) throw new VoogRequestException('Asset '.$filename.' not found.');
			if (sizeof($assetList)>1) throw new VoogRequestException('Multiple matches for '.$filename.' found.');

			// Return
			foreach ($assetList as $asset)
			{
				return $this->getAsset(strval($asset->id));
			}
		}


		/**
		 *
		 *   Upload asset
		 *   ------------
		 *   @access public
		 *   @param string $sourceFilename Source file name
		 *   @param string $filename File name at Voog
		 *   @param string $contentType MIME content type
		 *   @param int $media_set_id Media set ID (optional)
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function uploadAsset( $sourceFilename, $filename=null, $contentType=null, $media_set_id=null )
		{
			// Check
			if (empty($sourceFilename)) throw new VoogParamException('Missing input data: $sourceFilename.');
			if (!is_readable($sourceFilename)) throw new VoogParamException('Source file not readable.');

			// Check/get filename
			if (empty($filename))
			{
				$filename=pathinfo($sourceFilename,PATHINFO_BASENAME);
			}

			// Check/get content-type
			if (empty($contentType))
			{
				$contentType=HttpRequest::getMimeType($filename);
			}

			// Initialize file upload
			$uploadInitData=array();
			$uploadInitData['filename']=$filename;
			$uploadInitData['content_type']=$contentType;
			$uploadInitData['size']=filesize($sourceFilename);
			$voogUploadInitResponse=$this->VoogPHP->makeAPIrequest('/assets','POST',$uploadInitData);

			// Check
			if (empty($voogUploadInitResponse->id)) throw new VoogRequestException('Upload init failed: no id in response.');
			if (empty($voogUploadInitResponse->upload_url)) throw new VoogRequestException('Upload init failed: no upload URL in response.');
			if (empty($voogUploadInitResponse->confirm_url)) throw new VoogRequestException('Upload init failed: no confirm URL in response.');

			// If this fails, we need to delete the original file
			try
			{
				// Upload file
				try
				{
					$HTTP=new HttpRequest();
					$HTTP->setURL($voogUploadInitResponse->upload_url);
					$HTTP->setRequestMethod('PUT');
					$HTTP->setRawPostData(file_get_contents($sourceFilename));
					$HTTP->setRequestHeader('x-amz-acl','public-read');
					$HTTP->setRequestHeader('Content-Type',$contentType);
					$amazonUploadResponse=$HTTP->send();
				}
				catch (\Exception $e)
				{
					throw new VoogRequestException('HTTP error talking to Amazon: '.$e->getMessage());
				}

				// Check response
				if ($HTTP->getResponseCode()<200 || $HTTP->getResponseCode()>299)
				{
					// Try to see if we have an error
					if (mb_strlen($amazonUploadResponse))
					{
						try
						{
							$amazonUploadResponse=new \SimpleXMLElement($amazonUploadResponse);
						}
						catch (\Exception $e)
						{
							throw new VoogRequestException('Error uploading file: unable to parse Amazon response.');
						}
						$errorMessage=(!empty(strval($amazonUploadResponse->Code))?strval($amazonUploadResponse->Code).' - ':'').strval($amazonUploadResponse->Message);
						throw new VoogRequestException('Error from Amazon: '.$errorMessage);
					}
					else
					{
						throw new VoogRequestException('Error uploading file: got HTTP '.$HTTP->getResponseCode().' error from Amazon.');
					}
				}

				// Confirm
				$voogConfirmResponse=$this->VoogPHP->makeAPIrequest('/assets/'.strval($voogUploadInitResponse->id).'/confirm','PUT');

				// Move to media set?
				if (!empty($voogConfirmResponse->id) && !empty($media_set_id))
				{
					$this->VoogPHP->MediaSets->addAssetsToMediaset($media_set_id,array(strval($voogConfirmResponse->id)));
				}

				return $voogConfirmResponse;
			}
			catch (\Exception $e)
			{
				$error=$e->getMessage();
				try
				{
					$this->VoogPHP->makeAPIrequest('/assets/'.strval($voogUploadInitResponse->id),'DELETE');
				}
				catch (\Exception $e)
				{
					throw new VoogRequestException($error.' // Additionally, trying to delete the pending asset failed.');
				}
				throw new VoogRequestException($error);
			}
		}


		/**
		 *
		 *   Delete asset
		 *   ------------
		 *   @access public
		 *   @param int $id Page ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteAsset( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Delete
			return $this->VoogPHP->makeAPIrequest('/assets/'.strval($id),'DELETE');
		}


		/**
		 *
		 *   Get asset public URL for given size
		 *   -----------------------------------
		 *   @access public
		 *   @param object $assetObject Asset info object
		 *   @param array $sizeArray Size preference array
		 *   @param bool $protocolLess Return protocol-less link
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getAssetUrlForSize( $assetObject, $sizeArray=array(), $protocolLess=true )
		{
			// Check
			if (!is_object($assetObject)) throw new VoogParamException('Input parameter $assetObject not an object.');
			if (empty($assetObject->id)) throw new VoogParamException('Input parameter $assetObject does not look like an asset object.');
			if (empty($assetObject->size)) throw new VoogParamException('Input parameter $assetObject does not look like an asset object.');

			// Check for specific size
			$url='';
			if (!empty($assetObject->sizes))
			{
				foreach ($sizeArray as $size)
				{
					foreach ($assetObject->sizes as $assetSize)
					{
						if (strval($assetSize->thumbnail)==$size)
						{
							$url=strval($assetSize->public_url);
							break 2;
						}
					}
				}
			}

			// If nothing found, then use full size url
			if (empty($url))
			{
				$url=strval($assetObject->public_url);
			}

			// Remove protocol
			if ($protocolLess)
			{
				$url=preg_replace("/^([A-Za-z]+):/","",$url);
			}

			// Return
			return $url;
		}


	}


?>