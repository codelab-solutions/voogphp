<?php


	/**
	 *
	 *   Voog PHP API
	 *   Content partial functions
	 *   -------------------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class ContentPartials extends VoogPHPComponent
	{


		/**
		 *
		 *   Get content partials
		 *   --------------------
		 *   @access public
		 *   @param string $parent_type Parent type
		 *   @param int $parent_id Parent ID
		 *   @param array|object $data Additional filter data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getContentPartials( $parent_type=null, $parent_id=null, $data=null )
		{
			// Build request
			$requestParam=array();
			if (!empty($parent_type) || !empty($parent_id))
			{
				if (empty($parent_type)) throw new VoogParamException('Missing input data: $parent_type.');
				if (empty($parent_id)) throw new VoogParamException('Missing input data: $parent_id.');
				$requestParam['parent_type']=$parent_type;
				$requestParam['parent_id']=$parent_id;
			}
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestParam[$k]=$v;
				}
			}

			// Fetch
			$voogResponse=$this->VoogPHP->makeAPIrequest('/content_partials?'.http_build_query($requestParam));

			// Build response array
			$contentPartialsArray=array();
			foreach ($voogResponse as $contentPartial)
			{
				$contentPartialsArray[strval($contentPartial->id)]=$contentPartial;
			}

			// Return
			return $contentPartialsArray;
		}


		/**
		 *
		 *   Get a single content partial
		 *   ----------------------------
		 *   @access public
		 *   @param int $id Content partial ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getContentPartial( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/content_partials/'.strval($id));
		}


		/**
		 *
		 *   Update a content partial
		 *   ------------------------
		 *   @access public
		 *   @param int $id Content partial ID
		 *   @param array|object $data Update data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function updateContentPartial( $id, $data )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');
			if (empty($data)) throw new VoogParamException('Missing input data: $data.');
			if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');

			// Create data
			$requestData=new \stdClass();
			foreach ($data as $k => $v)
			{
				$requestData->{$k}=$v;
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/content_partials/'.strval($id),'PUT',$requestData);
		}


		/**
		 *
		 *   Delete a content partial
		 *   ------------------------
		 *   @access public
		 *   @param int $id Content partial ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteContentPartial( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Delete
			return $this->VoogPHP->makeAPIrequest('/content_partials/'.strval($id),'DELETE');
		}


	}


?>