<?php


	/**
	 *
	 *   Voog PHP API
	 *   Invoice functions
	 *   -----------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class Invoices extends VoogPHPComponent
	{


		/**
		 *
		 *   Get invoice PDF
		 *   ---------------
		 *   @access public
		 *   @param int $id Layout ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return string
		 *
		 */

		public function getInvoice( $uuid )
		{
			// Check
			if (empty($uuid)) throw new VoogParamException('Missing input data: $uuid.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/invoices/'.strval($uuid));
		}


	}


?>