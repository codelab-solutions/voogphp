<?php


	/**
	 *
	 *   Voog PHP API
	 *   Shipping method functions
	 *   -------------------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class ShippingMethods extends VoogPHPComponent
	{


		/**
		 *
		 *   Get shipping methods
		 *   --------------------
		 *   @access public
		 *   @param bool $includeTranslations Include translations
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getShippingMethods( $languageCode=null, $includeTranslations=false )
		{
			// Request
			$requestParam=array();
			if ($languageCode) $requestParam['language_code']=$languageCode;
			if ($includeTranslations) $requestParam['include']='translations';

			// Fetch list
			$voogResponse=$this->VoogPHP->makeAPIrequest('/ecommerce/v1/shipping_methods?'.http_build_query($requestParam));

			// Build response array
			$shippingMethodsArray=array();
			foreach ($voogResponse as $shippingMethod)
			{
				$shippingMethodsArray[strval($shippingMethod->id)]=$shippingMethod;
			}

			// Return
			return $shippingMethodsArray;
		}


		/**
		 *
		 *   Get a specific shipping method
		 *   ------------------------------
		 *   @access public
		 *   @param int $id Shipping method ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getShippingMethod( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/shipping_methods/'.strval($id));
		}


		/**
		 *
		 *   Add a new shipping method
		 *   -------------------------
		 *   @access public
		 *   @param string $name Shipping method name
		 *   @param array $data Additional attributes
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function addShippingMethod( $name, $data=null )
		{
			// Check
			if (empty($name)) throw new VoogParamException('Missing input data: $name.');

			// Create data
			$requestData=new \stdClass();
			$requestData->name=$name;
			if (!empty($data))
			{
				foreach ($data as $k => $v)
				{
					$requestData->{$k}=$v;
				}
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/shipping_methods','POST',$requestData);
		}


		/**
		 *
		 *   Update shipping method attributes
		 *   ---------------------------------
		 *   @access public
		 *   @param int $id Shipping method ID
		 *   @param array $data Additional attributes
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function updateShippingMethod( $id, $data )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');
			if (empty($data)) throw new VoogParamException('Missing input data: $data.');
			if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');

			// Create data
			$requestData=new \stdClass();
			if (!empty($data))
			{
				foreach ($data as $k => $v)
				{
					$requestData->{$k}=$v;
				}
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/shipping_methods/'.strval($id),'PUT',$requestData);
		}


		/**
		 *
		 *   Delete a shipping method
		 *   ------------------------
		 *   @access public
		 *   @param int $id Shipping method ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteShippingMethod( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Delete
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/shipping_methods/'.strval($id),'DELETE');
		}


	}


?>