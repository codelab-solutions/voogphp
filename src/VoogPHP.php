<?php


	/**
	 *
	 *   Voog PHP API
	 *   A PHP implementation of the Voog CMS platform API
	 *   -------------------------------------------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;
	use Codelab\FlaskHTTP\HttpRequest;
	use Codelab\FlaskHTTP\HttpRequestPart;


	class VoogPHP
	{


		/**
		 *   Site URL
		 *   @var string
		 */

		public $siteURL = null;


		/**
		 *   API token
		 *   @var string
		 */

		public $apiToken = null;


		/**
		 *   Log?
		 *   @var string
		 */

		public $log = null;


		/**
		 *   Site functions
		 *   @var Site
		 */

		public $Site = null;


		/**
		 *   User functions
		 *   @var Users
		 */

		public $Users = null;


		/**
		 *   Admin user functions
		 *   @var AdminUsers
		 */

		public $AdminUsers = null;


		/**
		 *   Language functions
		 *   @var Languages
		 */

		public $Languages = null;


		/**
		 *   Menu node functions
		 *   @var Nodes
		 */

		public $Nodes = null;


		/**
		 *   Page layout functions
		 *   @var Layouts
		 */

		public $Layouts = null;


		/**
		 *   Page functions
		 *   @var Pages
		 */

		public $Pages = null;


		/**
		 *   Article functions
		 *   @var Articles
		 */

		public $Articles = null;


		/**
		 *   Element functions
		 *   @var Elements
		 */

		public $Elements = null;


		/**
		 *   Element definition functions
		 *   @var ElementDefinitions
		 */

		public $ElementDefinitions = null;


		/**
		 *   Media set functions
		 *   @var MediaSets
		 */

		public $MediaSets = null;


		/**
		 *   Asset functions
		 *   @var Assets
		 */

		public $Assets = null;


		/**
		 *   Text content functions
		 *   @var Texts
		 */

		public $Texts = null;


		/**
		 *   Form functions
		 *   @var Forms
		 */

		public $Forms = null;


		/**
		 *   Content partial functions
		 *   @var ContentPartials
		 */

		public $ContentPartials = null;


		/**
		 *   Buy button functions
		 *   @var BuyButtons
		 */

		public $BuyButtons = null;


		/**
		 *   General store functions
		 *   @var Store
		 */

		public $Store = null;


		/**
		 *   Store product functions
		 *   @var Products
		 */

		public $Products = null;


		/**
		 *   Store discount functions
		 *   @var Discounts
		 */

		public $Discounts = null;


		/**
		 *   Shopping cart functions
		 *   @var Carts
		 */

		public $Carts = null;


		/**
		 *   Order functions
		 *   @var Orders
		 */

		public $Orders = null;


		/**
		 *   Invoice functions
		 *   @var Invoices
		 */

		public $Invoices = null;


		/**
		 *   Shipping method functions
		 *   @var ShippingMethods
		 */

		public $ShippingMethods = null;


		/**
		 *   Payment gateway functions
		 *   @var PaymentGateways
		 */

		public $PaymentGateways = null;


		/**
		 *
		 *   Init API instance
		 *   -----------------
		 *   @access public
		 *   @param string $siteURL Site URL
		 *   @param string $apiToken API token
		 *   @param string $log Log file
		 *   @throws VoogInitException
		 *   @return VoogPHP
		 *
		 */

		public function __construct( $siteURL, $apiToken, $log=null )
		{
			// Validate
			if (!mb_strlen($siteURL)) throw new VoogInitException('Cannot init Voog API: $siteURL not specified.');
			if (!mb_strlen($apiToken)) throw new VoogInitException('Cannot init Voog API: $apiToken not specified.');
			if (!function_exists('curl_init')) throw new VoogInitException('Cannot init Voog API: cURL not installed.');

			// Set up parameters
			$this->siteURL=$siteURL;
			$this->apiToken=$apiToken;
			$this->log=$log;

			// Init classes
			$this->Site=new Site($this);
			$this->Users=new Users($this);
			$this->AdminUsers=new AdminUsers($this);
			$this->Languages=new Languages($this);
			$this->Nodes=new Nodes($this);
			$this->Layouts=new Layouts($this);
			$this->Pages=new Pages($this);
			$this->Articles=new Articles($this);
			$this->Elements=new Elements($this);
			$this->ElementDefinitions=new ElementDefinitions($this);
			$this->MediaSets=new MediaSets($this);
			$this->Assets=new Assets($this);
			$this->Texts=new Texts($this);
			$this->Forms=new Forms($this);
			$this->ContentPartials=new ContentPartials($this);
			$this->BuyButtons=new BuyButtons($this);
			$this->Store=new Store($this);
			$this->Products=new Products($this);
			$this->Discounts=new Discounts($this);
			$this->Carts=new Carts($this);
			$this->Orders=new Orders($this);
			$this->Invoices=new Invoices($this);
			$this->ShippingMethods=new ShippingMethods($this);
			$this->PaymentGateways=new PaymentGateways($this);
		}


		/**
		 *
		 *   Make API request
		 *   ----------------
		 *   @access public
		 *   @param string $url URL
		 *   @param string $method Request method (GET/POST/PUT/PATCH/DELETE)
		 *   @param string|array|object $data Request data
		 *   @param string $contentType Content-type
		 *   @param array $fileUploads File uploads
		 *   @throws VoogRequestException
		 *   @return array|object
		 *
		 */

		public function makeAPIrequest( $url, $method='GET', $data=null, $contentType=null, $fileUploads=null )
		{
			try
			{
				// Set up request
				$HTTP=new HttpRequest();
				$HTTP->setURL($this->siteURL.'/admin/api'.(strncmp($url,'/',1)?'/':'').$url);
				$HTTP->setRequestMethod(mb_strtoupper($method));
				$HTTP->setRequestHeader('X-API-TOKEN',$this->apiToken);
				if (in_array(mb_strtoupper($method),array('GET')))
				{
					if (!empty($data))
					{
						if (is_array($data))
						{
							$HTTP->setURL($HTTP->URL.'?'.http_build_query($data));
						}
						else
						{
							$HTTP->setURL($HTTP->URL.'?'.$data);
						}
					}
				}
				else
				{
					if (!empty($fileUploads))
					{
						$HTTP->setMultiPart(true);
						if (!empty($data))
						{
							if (is_array($data) || is_object($data))
							{
								$requestData=json_encode($data,JSON_HEX_QUOT);
							}
							else
							{
								$requestData=$data;
							}
						}
						else
						{
							$requestData='{}';
						}
						$Part=new HttpRequestPart($requestData,'application/json',array('name'=>'request'));
						$HTTP->addPart($Part);
						foreach ($fileUploads as $fileID => $filename)
						{
							$Part=new HttpRequestPart(file_get_contents($filename),null,array('name'=>basename($filename),'filename'=>basename($filename)),true);
							$HTTP->addPart($Part);
						}
					}
					else
					{
						$HTTP->setRequestHeader('Content-Type',(!empty($contentType)?$contentType:'application/json'));
						if (is_array($data) || is_object($data))
						{
							$HTTP->setRawPostData(json_encode($data,JSON_HEX_QUOT));
						}
						else
						{
							$HTTP->setRawPostData($data);
						}
					}
				}
				if (!empty($fileUploads))
				{
					foreach ($fileUploads as $fileID => $filename)
					{
						$HTTP->setFileUpload($filename,null,null,$fileID);
					}
				}

				// Make request
				$voogResponse=$HTTP->send();

				// Log
				if ($this->log)
				{
					$request="\n\n"."----------"."\n\n";
					$request.="Date/time: ".date('Y-m-d H:i:s')."\n";
					$request.="URL: ".$HTTP->URL."\n";
					$request.="Request method: ".$HTTP->requestMethod."\n";
					$request.="Request: ".$HTTP->rawPostData."\n";
					$request.="Response: ".$voogResponse."\n";
					$request.="Response status: ".$HTTP->getResponseCode()."\n";
					file_put_contents($this->log,$request,FILE_APPEND);
				}
			}
			catch (\Exception $e)
			{
				// Log
				if ($this->log)
				{
					$request="\n\n"."----------"."\n\n";
					$request.="Date/time: ".date('Y-m-d H:i:s')."\n";
					$request.="URL: ".$HTTP->URL."\n";
					$request.="Request method: ".$HTTP->requestMethod."\n";
					$request.="Request: ".$HTTP->rawPostData."\n";
					$request.="Error: ".$e->getMessage()."\n";
					$request.="Response status: ".$HTTP->getResponseCode()."\n";
					file_put_contents($this->log,$request,FILE_APPEND);
				}

				// Rethrow
				throw new VoogRequestException($e->getMessage());
			}

			// Check response
			if ($HTTP->getResponseCode()<200 || $HTTP->getResponseCode()>299)
			{
				if (mb_strlen($voogResponse))
				{
					$voogResponseObj=json_decode($voogResponse);
					if ($voogResponseObj===false)
					{
						throw new VoogRequestException('Non-200 response from VOOG API: '.$HTTP->getResponseCode().' - '.$voogResponse,$HTTP->getResponseCode());
					}
					else
					{
						throw new VoogRequestException('Error from VOOG API: '.$HTTP->getResponseCode().' - '.(!empty($voogResponseObj->message)?$voogResponseObj->message:$voogResponseObj->error),$HTTP->getResponseCode());
					}
				}
				else
				{
					throw new VoogRequestException('Non-200 response from VOOG API: '.$HTTP->getResponseCode(),$HTTP->getResponseCode());
				}
			}

			// Empty response
			if ($HTTP->getResponseCode()==204)
			{
				return null;
			}

			// Parse response
			if (mb_strlen($voogResponse))
			{
				$voogResponse=json_decode($voogResponse);
				if ($voogResponse===false) throw new VoogRequestException('Could not parse response JSON.',1001);
			}

			// Return
			return $voogResponse;
		}


	}


?>