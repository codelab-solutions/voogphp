<?php


	/**
	 *
	 *   Voog PHP API
	 *   The base Voog exception class
	 *   -----------------------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class VoogException extends \Exception
	{
	}


?>