<?php


	/**
	 *
	 *   Voog PHP API
	 *   Language functions
	 *   ------------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class Languages extends VoogPHPComponent
	{


		/**
		 *
		 *   Get languages
		 *   -------------
		 *   @access public
		 *   @param array $data Request data
		 *   @param bool $listOnly Return displayable list only
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getLanguages( $data=null, $listOnly=false )
		{
			// Request parameters
			$requestParam=array();
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestParam[$k]=$v;
				}
			}

			// Fetch
			$voogResponse=$this->VoogPHP->makeAPIrequest('/languages?'.http_build_query($requestParam));

			// Build response array
			$languageArray=array();
			foreach ($voogResponse as $lang)
			{
				if ($listOnly)
				{
					$languageArray[strval($lang->code)]=strval($lang->title);
				}
				else
				{
					$languageArray[strval($lang->id)]=$lang;
				}
			}

			// Return
			return $languageArray;
		}


		/**
		 *
		 *   Get language
		 *   ------------
		 *   @access public
		 *   @param int $id Page ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getLanguage( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/languages/'.strval($id));
		}


		/**
		 *
		 *   Get language by code
		 *   --------------------
		 *   @access public
		 *   @param string $code ISO-639-1 Language code
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getLanguageByCode( $code )
		{
			// Check
			if (empty($code)) throw new VoogParamException('Missing input data: $code.');

			// Get list
			$languageList=$this->getLanguages(array('q.language.code.$eq'=>$code),false);
			if (!sizeof($languageList)) throw new VoogRequestException('Language '.$code.' not found.');
			if (sizeof($languageList)>1) throw new VoogRequestException('Multiple matches for '.$code.' found.');

			// Return
			foreach ($languageList as $lang)
			{
				return $lang;
			}
		}


		/**
		 *
		 *   Enable autodetect
		 *   -----------------
		 *   @access public
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function enableAutoDetect()
		{
			// Make request
			return $this->VoogPHP->makeAPIrequest('/languages/enable_autodetect','PUT');
		}


		/**
		 *
		 *   Add a new language
		 *   ------------------
		 *   @access public
		 *   @param string $code ISO-639-1 Language code
		 *   @param string $title Language title
		 *   @param array $data Additional language data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function addLanguage( $code, $title, $data=null )
		{
			// Check
			if (empty($code)) throw new VoogParamException('Missing input data: $code.');
			if (empty($title)) throw new VoogParamException('Missing input data: $title.');

			// Create data
			$requestData=new \stdClass();
			$requestData->code=$code;
			$requestData->title=$title;
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestData->{$k}=$v;
				}
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/languages','POST',$requestData);
		}


		/**
		 *
		 *   Update language attributes
		 *   --------------------------
		 *   @access public
		 *   @param int $id Language ID
		 *   @param object|array $data Language data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function updateLanguage( $id, $data )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');
			if (empty($data)) throw new VoogParamException('Missing input data: $data.');
			if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');

			// Create data
			$requestData=new \stdClass();
			foreach ($data as $k => $v)
			{
				$requestData->{$k}=$v;
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/languages/'.strval($id),'PUT',$requestData);
		}


		/**
		 *
		 *   Move language before another
		 *   ----------------------------
		 *   @access public
		 *   @param int $id Language ID
		 *   @param int $before Language ID, before what to move
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function moveLanguageBefore( $id, $before )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');
			if (empty($before)) throw new VoogParamException('Missing input data: $before.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/languages/'.strval($id).'/move?before='.intval($before),'PUT');
		}


		/**
		 *
		 *   Move language after another
		 *   ---------------------------
		 *   @access public
		 *   @param int $id Language ID
		 *   @param int $after Language ID, after what to move
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function moveLanguageAfter( $id, $after )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');
			if (empty($after)) throw new VoogParamException('Missing input data: $after.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/languages/'.strval($id).'/move?after='.intval($after),'PUT');
		}


		/**
		 *
		 *   Delete a language
		 *   -----------------
		 *   @access public
		 *   @param int $id Language ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteLanguage( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Delete
			return $this->VoogPHP->makeAPIrequest('/languages/'.strval($id),'DELETE');
		}


		/**
		 *
		 *   Get language contents
		 *   ---------------------
		 *   @access public
		 *   @param int $id Language ID
		 *   @param array $data Request data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getLanguageContents( $id, $data=null )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Request with parameters
			$requestParam=array();
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestParam[$k]=$v;
				}
			}

			// Fetch list
			$voogResponse=$this->VoogPHP->makeAPIrequest('/languages/'.strval($id).'/contents?'.http_build_query($requestParam),'GET');

			// Build response array
			$contentsArray=array();
			foreach ($voogResponse as $content)
			{
				$contentsArray[strval($content->id)]=$content;
			}

			// Return
			return $contentsArray;
		}


		/**
		 *
		 *   Get language content
		 *   --------------------
		 *   @access public
		 *   @param int $language_id Language ID
		 *   @param int $content_id Content ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getLanguageContent( $language_id, $content_id )
		{
			// Check
			if (empty($language_id)) throw new VoogParamException('Missing input data: $language_id.');
			if (empty($content_id)) throw new VoogParamException('Missing input data: $content_id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/languages/'.strval($language_id).'/contents/'.strval($content_id));
		}


		/**
		 *
		 *   Create a language content item
		 *   ------------------------------
		 *   @access public
		 *   @param int $language_id Language ID
		 *   @param array $data Content data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function addLanguageContent( $language_id, $data=null )
		{
			// Check
			if (empty($language_id)) throw new VoogParamException('Missing input data: $language_id.');
			if (empty($data)) throw new VoogParamException('Missing input data: $data.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/languages/'.strval($language_id).'/contents','POST',$data);
		}


		/**
		 *
		 *   Delete language content item
		 *   ----------------------------
		 *   @access public
		 *   @param int $language_id Language ID
		 *   @param int $content_id Content ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteLanguageContent( $language_id, $content_id )
		{
			// Check
			if (empty($language_id)) throw new VoogParamException('Missing input data: $language_id.');
			if (empty($content_id)) throw new VoogParamException('Missing input data: $content_id.');

			// Delete
			return $this->VoogPHP->makeAPIrequest('/languages/'.strval($language_id).'/contents/'.strval($content_id),'DELETE');
		}


	}


?>