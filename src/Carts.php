<?php


	/**
	 *
	 *   Voog PHP API
	 *   Shopping cart functions
	 *   -----------------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class Carts extends VoogPHPComponent
	{


		/**
		 *
		 *   Create a shopping cart
		 *   ----------------------
		 *   @access public
		 *   @param array $data Additional language data
		 *   @param bool $includeItems Include item lines
		 *   @param bool $includeTranslations Include translations
		 *   @param bool $includeShippingAddress Include shipping address
		 *   @param bool $includeBillingAddress Include billing address
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function createCart( $data, $includeItems=false, $includeTranslations=false, $includeShippingAddress=false, $includeBillingAddress=false )
		{
			// Check
			if (empty($data)) throw new VoogParamException('Missing input data: $data.');
			if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');

			// Request param
			$requestParam=array();
			$include=array();
			if ($includeItems) $include[]='items';
			if ($includeTranslations) $include[]='translations';
			if ($includeShippingAddress) $include[]='shipping_address';
			if ($includeBillingAddress) $include[]='billing_address';
			if (!empty($include))
			{
				$requestParam['include']=join(',',$include);
			}

			// Create data
			$requestData=new \stdClass();
			foreach ($data as $k => $v)
			{
				$requestData->{$k}=$v;
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/carts?'.http_build_query($requestParam),'POST',$requestData);
		}


		/**
		 *
		 *   Get shopping cart content
		 *   -------------------------
		 *   @access public
		 *   @param string $uuid Cart UUID
		 *   @param bool $includeItems Include item lines
		 *   @param bool $includeTranslations Include translations
		 *   @param bool $includeShippingAddress Include shipping address
		 *   @param bool $includeBillingAddress Include billing address
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getCart( $uuid, $includeItems=false, $includeTranslations=false, $includeShippingAddress=false, $includeBillingAddress=false )
		{
			// Check
			if (empty($uuid)) throw new VoogParamException('Missing input data: $uuid.');

			// Request param
			$requestParam=array();
			$include=array();
			if ($includeItems) $include[]='items';
			if ($includeTranslations) $include[]='translations';
			if ($includeShippingAddress) $include[]='shipping_address';
			if ($includeBillingAddress) $include[]='billing_address';
			if (!empty($include))
			{
				$requestParam['include']=join(',',$include);
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/carts/'.strval($uuid).'?'.http_build_query($requestParam));
		}


		/**
		 *
		 *   Update shopping cart
		 *   --------------------
		 *   @access public
		 *   @param string $uuid Cart UUID
		 *   @param array $data Additional language data
		 *   @param bool $includeItems Include item lines
		 *   @param bool $includeTranslations Include translations
		 *   @param bool $includeShippingAddress Include shipping address
		 *   @param bool $includeBillingAddress Include billing address
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function updateCart( $uuid, $data, $includeItems=false, $includeTranslations=false, $includeShippingAddress=false, $includeBillingAddress=false )
		{
			// Check
			if (empty($uuid)) throw new VoogParamException('Missing input data: $uuid.');
			if (empty($data)) throw new VoogParamException('Missing input data: $data.');
			if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');

			// Request param
			$requestParam=array();
			$include=array();
			if ($includeItems) $include[]='items';
			if ($includeTranslations) $include[]='translations';
			if ($includeShippingAddress) $include[]='shipping_address';
			if ($includeBillingAddress) $include[]='billing_address';
			if (!empty($include))
			{
				$requestParam['include']=join(',',$include);
			}

			// Create data
			$requestData=new \stdClass();
			foreach ($data as $k => $v)
			{
				$requestData->{$k}=$v;
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/carts/'.strval($uuid).'?'.http_build_query($requestParam),'PUT',$requestData);
		}


		/**
		 *
		 *   Start checkout process
		 *   ----------------------
		 *   @access public
		 *   @param string $uuid Cart UUID
		 *   @param array $data Additional language data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function startCheckout( $uuid, $data=null )
		{
			// Check
			if (empty($uuid)) throw new VoogParamException('Missing input data: $uuid.');

			// Create data
			$requestData=new \stdClass();
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestData->{$k}=$v;
				}
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/carts/'.strval($uuid).'/checkout','POST',$requestData);
		}


	}


?>