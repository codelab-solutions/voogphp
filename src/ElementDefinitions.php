<?php


	/**
	 *
	 *   Voog PHP API
	 *   Element definition functions
	 *   ----------------------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class ElementDefinitions extends VoogPHPComponent
	{


		/**
		 *
		 *   Get element definitions
		 *   -----------------------
		 *   @access public
		 *   @param array $data Request data
		 *   @param bool $listOnly Return displayable list only
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getElementDefinitions( $data=null, $listOnly=false )
		{
			// Request parameters
			$requestParam=array();
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestParam[$k]=$v;
				}
			}

			// Fetch
			$voogResponse=$this->VoogPHP->makeAPIrequest('/element_definitions?'.http_build_query($requestParam));

			// Build response array
			$elementDefArray=array();
			foreach ($voogResponse as $elementDef)
			{
				$elementDefArray[strval($elementDef->id)]=($listOnly?strval($elementDef->title):$elementDef);
			}

			// Return
			return $elementDefArray;
		}


		/**
		 *
		 *   Get element definition
		 *   ----------------------
		 *   @access public
		 *   @param int $id Element definition ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getElementDefinition( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/element_definitions/'.strval($id));
		}


		/**
		 *
		 *   Get element definition by name
		 *   ------------------------------
		 *   @access public
		 *   @param string $name Element definition name
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getElementDefinitionByName( $name )
		{
			// Check
			if (empty($name)) throw new VoogParamException('Missing input data: $name.');

			// Get list
			$elementDefinitionList=$this->getElementDefinitions(array('q.element_definition.title.$eq'=>$name),false);
			if (!sizeof($elementDefinitionList)) throw new VoogRequestException('Element definition '.$name.' not found.');
			if (sizeof($elementDefinitionList)>1) throw new VoogRequestException('Multiple matches for '.$name.' found.');

			// Return
			foreach ($elementDefinitionList as $elementDefinition)
			{
				return $this->getElementDefinition(strval($elementDefinition->id));
			}
		}


		/**
		 *
		 *   Create an element definition
		 *   ----------------------------
		 *   @access public
		 *   @param string $title Title
		 *   @param array $fields Fields
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function addElementDefinition( $title, $fields )
		{
			// Check
			if (empty($title)) throw new VoogParamException('Missing input data: $title.');
			if (empty($fields)) throw new VoogParamException('Missing input data: $fields.');

			// Create data
			$requestData=new \stdClass();
			$requestData->title=$title;
			$requestData->fields=array();
			$fldPos=0;
			foreach ($fields as $key => $fieldData)
			{
				// Field position
				$fldPos++;

				// Check
				if (!is_object($fieldData) && !is_array($fieldData)) throw new VoogParamException('Field '.$key.' not an array or an object.');
				$fieldData=(object)$fieldData;
				if (empty($fieldData->key))
				{
					if (is_numeric($key)) throw new VoogParamException('Field '.$key.' does not contain a required key attribute.');
					$fieldData->key=$key;
				}
				if (empty($fieldData->title)) throw new VoogParamException('Field '.$key.' does not contain a required key attribute.');
				if (empty($fieldData->data_type)) throw new VoogParamException('Field '.$key.' does not contain a required data_type attribute.');
				$fieldData->position=$fldPos;
				$requestData->fields[]=$fieldData;
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/element_definitions','POST',$requestData);
		}


		/**
		 *
		 *   Delete element definition
		 *   -------------------------
		 *   @access public
		 *   @param int $id Element definition ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteElementDefinition( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Delete
			return $this->VoogPHP->makeAPIrequest('/element_definitions/'.strval($id),'DELETE');
		}


	}


?>