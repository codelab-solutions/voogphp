<?php


	/**
	 *
	 *   Voog PHP API
	 *   The Voog init exception
	 *   -----------------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class VoogInitException extends VoogException
	{
	}


?>