<?php


	/**
	 *
	 *   Voog PHP API
	 *   Media set functions
	 *   -------------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class MediaSets extends VoogPHPComponent
	{


		/**
		 *
		 *   Get media sets
		 *   --------------
		 *   @access public
		 *   @param array|object $data Filter data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getMediaSets( $data=null )
		{
			// Request parameters
			$requestParam=array();
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestParam[$k]=$v;
				}
			}

			// Fetch
			$voogResponse=$this->VoogPHP->makeAPIrequest('/media_sets?'.http_build_query($requestParam));

			// Build response array
			$mediasetArray=array();
			foreach ($voogResponse as $mediaset)
			{
				$mediasetArray[strval($mediaset->id)]=$mediaset;
			}

			// Return
			return $mediasetArray;
		}


		/**
		 *
		 *   Get media set
		 *   -------------
		 *   @access public
		 *   @param int $id Media set ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getMediaSet( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/media_sets/'.strval($id));
		}


		/**
		 *
		 *   Get media set by name
		 *   ---------------------
		 *   @access public
		 *   @param string $name Media set name
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getMediaSetByName( $name )
		{
			// Check
			if (empty($name)) throw new VoogParamException('Missing input data: $name.');

			// Get list
			$mediaSetList=$this->getMediaSets(array('q.media_set.title.$eq'=>$name));
			if (!sizeof($mediaSetList)) throw new VoogRequestException('Media set '.$name.' not found.');
			if (sizeof($mediaSetList)>1) throw new VoogRequestException('Multiple matches for '.$name.' found.');

			// Return
			foreach ($mediaSetList as $mediaset)
			{
				return $this->getMediaSet(strval($mediaset->id));
			}
		}


		/**
		 *
		 *   Add a new media set
		 *   -------------------
		 *   @access public
		 *   @param string $title Media set title
		 *   @param array $data Additional data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function addMediaSet( $title, $data=null )
		{
			// Check
			if (empty($title)) throw new VoogParamException('Missing input data: $title.');

			// Create data
			$requestData=new \stdClass();
			$requestData->title=$title;
			if (!empty($data))
			{
				foreach ($data as $k => $v)
				{
					$requestData->{$k}=$v;
				}
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/media_sets','POST',$requestData);
		}



		/**
		 *
		 *   Add assets to media set
		 *   -----------------------
		 *   @access public
		 *   @param int $id Media set ID
		 *   @param array $assetList List of asset IDs
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function addAssetsToMediaset( $id, $assetList )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');
			if (empty($assetList)) throw new VoogParamException('Missing input data: $assetList.');
			if (!is_array($assetList)) throw new VoogParamException('Invalid input data: $assetList.');

			// Object
			$requestData=new \stdClass();
			$requestData->asset_ids=$assetList;

			// Make request
			return $this->VoogPHP->makeAPIrequest('/media_sets/'.strval($id).'/add_assets','POST',$requestData);
		}


		/**
		 *
		 *   Update media set
		 *   ----------------
		 *   @access public
		 *   @param int $id Media set ID
		 *   @param array|object $data Update data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function updateMediaSet( $id, $data )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');
			if (empty($data)) throw new VoogParamException('Missing input data: $data.');
			if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');

			// Create data
			$requestData=new \stdClass();
			foreach ($data as $k => $v)
			{
				$requestData->{$k}=$v;
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/media_sets/'.strval($id),'PUT',$requestData);
		}


		/**
		 *
		 *   Delete media set
		 *   ----------------
		 *   @access public
		 *   @param int $id Media set ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteMediaSet( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Delete
			return $this->VoogPHP->makeAPIrequest('/media_sets/'.strval($id),'DELETE');
		}


	}


?>