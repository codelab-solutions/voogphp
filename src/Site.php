<?php


	/**
	 *
	 *   Voog PHP API
	 *   Site functions
	 *   --------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class Site extends VoogPHPComponent
	{


		/**
		 *
		 *   Get site data
		 *   -------------
		 *   @access public
		 *   @param bool $includeStatsInfo Include stats info
		 *   @param bool $includeSubscriptionInfo Include subscription info
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getSiteData( $includeStatsInfo=false, $includeSubscriptionInfo=false )
		{
			// Param
			$requestParam=array();
			if ($includeStatsInfo) $requestParam['include_stats']='true';
			if ($includeSubscriptionInfo) $requestParam['include_subscription']='true';

			// Request with param
			return $this->VoogPHP->makeAPIrequest('/site?'.http_build_query($requestParam));
		}


		/**
		 *
		 *   Update site data
		 *   ----------------
		 *   @access public
		 *   @param object $data Data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function updateSiteData( $data )
		{
			// Check
			if (empty($data)) throw new VoogParamException('Missing input data: $data.');
			if (!is_object($data)) throw new VoogParamException('Invalid input data: $data not an object.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/site','PATCH',$data);
		}


		/**
		 *
		 *   Set site data parameter
		 *   -----------------------
		 *   @access public
		 *   @param string $paramName Parameter name
		 *   @param mixed $paramValue Parameter value
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function setSiteCustomData( $paramName, $paramValue )
		{
			// Check
			if (empty($paramName)) throw new VoogParamException('Missing input data: $paramName.');

			// Request data
			$requestData=array(
				'value' => $paramValue
			);

			// Make request
			return $this->VoogPHP->makeAPIrequest('/site/data/'.$paramName,'PUT',$requestData);
		}


		/**
		 *
		 *   Remove site data parameter
		 *   --------------------------
		 *   @access public
		 *   @param string $paramName Parameter name
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteSiteCustomData( $paramName )
		{
			// Check
			if (empty($paramName)) throw new VoogParamException('Missing input data: $paramName.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/site/data/'.$paramName,'DELETE');
		}


	}


?>