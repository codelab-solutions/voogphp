<?php


	/**
	 *
	 *   Voog PHP API
	 *   Text content functions
	 *   ----------------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class Texts extends VoogPHPComponent
	{


		/**
		 *
		 *   Get text contents
		 *   -----------------
		 *   @access public
		 *   @param string $parent_type Parent type
		 *   @param int $parent_id Parent ID
		 *   @param array|object $data Additional filter data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getTexts( $parent_type=null, $parent_id=null, $data=null )
		{
			// Build request
			$requestParam=array();
			if (!empty($parent_type) || !empty($parent_id))
			{
				if (empty($parent_type)) throw new VoogParamException('Missing input data: $parent_type.');
				if (empty($parent_id)) throw new VoogParamException('Missing input data: $parent_id.');
				$requestParam['parent_type']=$parent_type;
				$requestParam['parent_id']=$parent_id;
			}
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestParam[$k]=$v;
				}
			}

			// Fetch
			$voogResponse=$this->VoogPHP->makeAPIrequest('/texts?'.http_build_query($requestParam));

			// Build response array
			$textArray=array();
			foreach ($voogResponse as $text)
			{
				$textArray[strval($text->id)]=$text;
			}

			// Return
			return $textArray;
		}


		/**
		 *
		 *   Get text content
		 *   ----------------
		 *   @access public
		 *   @param int $id Text ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getText( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/texts/'.strval($id));
		}


		/**
		 *
		 *   Update text content
		 *   -------------------
		 *   @access public
		 *   @param int $id Text ID
		 *   @param string $body Text body
		 *   @param array $data Additional text element data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 */

		public function updateText( $id, $body, $data=null )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Create data
			$requestData=new \stdClass();
			$requestData->body=strval($body);
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestData->{$k}=$v;
				}
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/texts/'.strval($id),'PUT',$requestData);
		}


		/**
		 *
		 *   Delete text content
		 *   -------------------
		 *   @access public
		 *   @param int $id Text ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteText( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/texts/'.strval($id),'DELETE');
		}


	}


?>