<?php


	/**
	 *
	 *   Voog PHP API
	 *   Article functions
	 *   -----------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class Articles extends VoogPHPComponent
	{


		/**
		 *
		 *   Get articles
		 *   ------------
		 *   @access public
		 *   @param int $page_id Page ID
		 *   @param array $tags List of tags
		 *   @param array|object $data Filter data
		 *   @param bool $includeTags Include tags in response
		 *   @param bool $includeDetails Include full details in response
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getArticles( $page_id=null, $tags=null, $data=null, $includeTags=false, $includeDetails=false )
		{
			// Request parameters
			$requestParam=array();
			if (!empty($page_id))
			{
				$requestParam['page_id']=$page_id;
			}
			if (!empty($tags))
			{
				foreach ($tags as $tag)
				{
					$requestParam['tag'.(sizeof($tags)>1?'[]':'')]=$tag;
				}
			}
			if (!empty($data))
			{
				foreach ($data as $k => $v)
				{
					$requestParam[$k]=$v;
				}
			}
			if ($includeTags)
			{
				$requestParam['include_tags']='true';
			}
			if ($includeDetails)
			{
				$requestParam['include_details']='true';
			}

			// Fetch
			$voogResponse=$this->VoogPHP->makeAPIrequest('/articles?'.http_build_query($requestParam));

			// Build response array
			$articleArray=array();
			foreach ($voogResponse as $article)
			{
				$articleArray[strval($article->id)]=$article;
			}

			// Return
			return $articleArray;
		}


		/**
		 *
		 *   Get article
		 *   -----------
		 *   @access public
		 *   @param int $id Article ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getArticle( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/articles/'.strval($id));
		}


		/**
		 *
		 *   Add an article (blog post)
		 *   --------------------------
		 *   @access public
		 *   @param int $page_id Page ID
		 *   @param string $title Title
		 *   @param string $excerpt Excerpt/lead
		 *   @param string $body Body
		 *   @param array|object $data Article parameters
		 *   @param array|object $articleData Additional key/value data
		 *   @param bool $publish Publish article
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function addArticle( $page_id, $title, $excerpt='', $body='', $data=null, $articleData=null, $publish=false )
		{
			// Check
			if (empty($page_id)) throw new VoogParamException('Missing input data: $page_id.');
			if (empty($title)) throw new VoogParamException('Missing input data: $title.');

			// Create data
			$requestData=new \stdClass();
			$requestData->page_id=intval($page_id);
			$requestData->autosaved_title=strval($title);
			if (!empty($excerpt)) $requestData->autosaved_excerpt=strval($excerpt);
			if (!empty($body)) $requestData->autosaved_body=strval($body);
			if (!empty($data) && (is_array($data) || is_object($data)))
			{
				foreach ($data as $k => $v)
				{
					$requestData->{$k}=$v;
				}
			}
			if (!empty($articleData) && (is_array($articleData) || is_object($articleData)))
			{
				$requestData->data=new \stdClass();
				foreach ($articleData as $k => $v)
				{
					$requestData->data->{$k}=$v;
				}
			}
			if ($publish)
			{
				$requestData->publishing=true;
				$requestData->published=true;
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/articles','POST',$requestData);
		}


		/**
		 *
		 *   Update an article (blog post)
		 *   -----------------------------
		 *   @access public
		 *   @param int $id Article ID
		 *   @param array|object $data Article parameters
		 *   @param array|object $articleData Additional key/value data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function updateArticle( $id, $data=null, $articleData=null )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');
			if (empty($data) && empty($articleData)) throw new VoogParamException('Missing input data: $data or $articleData.');

			// Create data
			$requestData=new \stdClass();
			if (!empty($data) && (is_array($data) || is_object($data)))
			{
				foreach ($data as $k => $v)
				{
					$requestData->{$k}=$v;
				}
			}
			if (!empty($articleData) && (is_array($articleData) || is_object($articleData)))
			{
				$requestData->data=new \stdClass();
				foreach ($articleData as $k => $v)
				{
					$requestData->data->{$k}=$v;
				}
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/articles/'.strval($id),'PATCH',$requestData);
		}


		/**
		 *
		 *   Delete article (blog post)
		 *   --------------------------
		 *   @access public
		 *   @param int $id Article ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteArticle( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Delete
			return $this->VoogPHP->makeAPIrequest('/articles/'.strval($id),'DELETE');
		}


		/**
		 *
		 *   Get article contents
		 *   --------------------
		 *   @access public
		 *   @param int $article_id Article ID
		 *   @param array $data Additional request data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getArticleContents( $article_id, $data=null )
		{
			// Check
			if (empty($article_id)) throw new VoogParamException('Missing input data: $article_id.');

			// Request parameters
			$requestParam=array();
			if (!empty($data))
			{
				foreach ($data as $k => $v)
				{
					$requestParam[$k]=$v;
				}
			}

			// Fetch list
			$voogResponse=$this->VoogPHP->makeAPIrequest('/articles/'.strval($article_id).'/contents?'.http_build_query($requestParam),'GET');

			// Build response array
			$contentsArray=array();
			foreach ($voogResponse as $content)
			{
				$contentsArray[strval($content->id)]=$content;
			}

			// Return
			return $contentsArray;
		}


		/**
		 *
		 *   Get article contents
		 *   --------------------
		 *   @access public
		 *   @param int $article_id Article ID
		 *   @param int $content_id Content ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getArticleContent( $article_id, $content_id )
		{
			// Check
			if (empty($article_id)) throw new VoogParamException('Missing input data: $article_id.');
			if (empty($content_id)) throw new VoogParamException('Missing input data: $content_id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/articles/'.strval($article_id).'/contents/'.strval($content_id));
		}


		/**
		 *
		 *   Add article content
		 *   -------------------
		 *   @access public
		 *   @param int $article_id Article ID
		 *   @param array $data Content data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function addArticleContent( $article_id, $data=null )
		{
			// Check
			if (empty($article_id)) throw new VoogParamException('Missing input data: $article_id.');
			if (empty($data)) throw new VoogParamException('Missing input data: $data.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/articles/'.strval($article_id).'/contents','POST',$data);
		}


		/**
		 *
		 *   Delete article content
		 *   ----------------------
		 *   @access public
		 *   @param int $article_id Article ID
		 *   @param int $content_id Content ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteArticleContent( $article_id, $content_id )
		{
			// Check
			if (empty($article_id)) throw new VoogParamException('Missing input data: $article_id.');
			if (empty($content_id)) throw new VoogParamException('Missing input data: $content_id.');

			// Delete
			return $this->VoogPHP->makeAPIrequest('/articles/'.strval($article_id).'/contents/'.strval($content_id),'DELETE');
		}


		/**
		 *
		 *   Set article custom data parameter
		 *   ---------------------------------
		 *   @access public
		 *   @param int $article_id Article ID
		 *   @param string $paramName Parameter name
		 *   @param mixed $paramValue Parameter value
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function setArticleCustomData( $article_id, $paramName, $paramValue )
		{
			// Check
			if (empty($article_id)) throw new VoogParamException('Missing input data: $article_id.');
			if (empty($paramName)) throw new VoogParamException('Missing input data: $paramName.');

			// Request
			$requestData=array(
				'value' => $paramValue
			);

			// Make request
			return $this->VoogPHP->makeAPIrequest('/articles/'.strval($article_id).'/data/'.$paramName,'PUT',$requestData);
		}


		/**
		 *
		 *   Remove article custom data parameter
		 *   ------------------------------------
		 *   @access public
		 *   @param int $article_id Article ID
		 *   @param string $paramName Parameter name
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteArticleCustomData( $article_id, $paramName )
		{
			// Check
			if (empty($article_id)) throw new VoogParamException('Missing input data: $article_id.');
			if (empty($paramName)) throw new VoogParamException('Missing input data: $paramName.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/articles/'.strval($article_id).'/data/'.$paramName,'DELETE');
		}


		/**
		 *
		 *   Get article tags
		 *   ----------------
		 *   @access public
		 *   @param int|string $lang Language ID or code
		 *   @param array|object $data Filter data
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getTags( $lang=null, $data=null )
		{
			// Request with parameters
			$requestParam=array();
			if (!empty($lang))
			{
				if (is_numeric($lang))
				{
					$requestParam['language_id']=$lang;
				}
				else
				{
					$requestParam['language_code']=$lang;
				}
			}
			if (!empty($data) && is_array($data))
			{
				foreach ($data as $k => $v)
				{
					$requestParam[$k]=$v;
				}
			}

			// Fetch list
			$voogResponse=$this->VoogPHP->makeAPIrequest('/tags?'.http_build_query($requestParam),'GET');

			// Build response array
			$tagsArray=array();
			foreach ($voogResponse as $tag)
			{
				$tagsArray[strval($tag->id)]=$tag;
			}

			// Return
			return $tagsArray;
		}


		/**
		 *
		 *   Get a single tag
		 *   ----------------
		 *   @access public
		 *   @param int $id Tag ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getTag( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/tags/'.strval($id));
		}


		/**
		 *
		 *   Get tag by name
		 *   ---------------
		 *   @access public
		 *   @param string $name Tag name
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getTagByName( $name )
		{
			// Check
			if (empty($name)) throw new VoogParamException('Missing input data: $name.');

			// Get list
			$tagsList=$this->getTags(null,array('q.tag.name.$eq'=>$name),false);
			if (!sizeof($tagsList)) throw new VoogRequestException('Tag '.$name.' not found.');
			if (sizeof($tagsList)>1) throw new VoogRequestException('Multiple matches for '.$name.' found.');

			// Return
			foreach ($tagsList as $tag)
			{
				return $tag;
			}
		}


		/**
		 *
		 *   Update a tag
		 *   ------------
		 *   @access public
		 *   @param int $id Tag ID
		 *   @param string $name New tag name
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function updateTag( $id, $name )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');
			if (empty($name)) throw new VoogParamException('Missing input data: $name.');

			// Create data
			$requestData=new \stdClass();
			if (!empty($body)) $requestData->name=$name;

			// Make request
			return $this->VoogPHP->makeAPIrequest('/tags/'.strval($id),'PUT',$requestData);
		}


		/**
		 *
		 *   Delete a tag
		 *   ------------
		 *   @access public
		 *   @param int $id Tag ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteTag( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Delete
			return $this->VoogPHP->makeAPIrequest('/tags/'.strval($id),'DELETE');
		}


		/**
		 *
		 *   Get article comments
		 *   --------------------
		 *   @access public
		 *   @param int $article_id Article ID
		 *   @param array|object $data Filter data
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getComments( $article_id, $data=null )
		{
			// Check
			if (empty($article_id)) throw new VoogParamException('Missing input data: $article_id.');

			// Request parameters
			$requestParam=array();
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v)
				{
					$requestParam[$k]=$v;
				}
			}

			// Fetch list
			$voogResponse=$this->VoogPHP->makeAPIrequest('/articles/'.strval($article_id).'/comments?'.http_build_query($requestParam),'GET');

			// Build response array
			$commentsArray=array();
			foreach ($voogResponse as $comment)
			{
				$commentsArray[strval($comment->id)]=$comment;
			}

			// Return
			return $commentsArray;
		}


		/**
		 *
		 *   Get a specific comment
		 *   ----------------------
		 *   @access public
		 *   @param int $article_id Article ID
		 *   @param int $comment_id Comment ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getComment( $article_id, $comment_id )
		{
			// Check
			if (empty($article_id)) throw new VoogParamException('Missing input data: $article_id.');
			if (empty($comment_id)) throw new VoogParamException('Missing input data: $comment_id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/articles/'.strval($article_id).'/comments/'.strval($comment_id));
		}


		/**
		 *
		 *   Create a comment
		 *   ---------------
		 *   @access public
		 *   @param int $article_id Article ID
		 *   @param string $author Author name
		 *   @param string $author_email Author's e-mail address
		 *   @param string $body Comment body
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function addComment( $article_id, $author, $author_email, $body )
		{
			// Check
			if (empty($article_id)) throw new VoogParamException('Missing input data: $article_id.');
			if (empty($author)) throw new VoogParamException('Missing input data: $author.');
			if (empty($author_email)) throw new VoogParamException('Missing input data: $author_email.');
			if (empty($body)) throw new VoogParamException('Missing input data: $body.');

			// Request data
			$requestData=new \stdClass();
			$requestData->author=$author;
			$requestData->author_email=$author_email;
			$requestData->body=$body;

			// Make request
			return $this->VoogPHP->makeAPIrequest('/articles/'.strval($article_id).'/comments','POST',$requestData);
		}


		/**
		 *
		 *   Delete article comment
		 *   ----------------------
		 *   @access public
		 *   @param int $article_id Article ID
		 *   @param int $comment_id Comment ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteComment( $article_id, $comment_id )
		{
			// Check
			if (empty($article_id)) throw new VoogParamException('Missing input data: $article_id.');
			if (empty($comment_id)) throw new VoogParamException('Missing input data: $comment_id.');

			// Delete
			return $this->VoogPHP->makeAPIrequest('/articles/'.strval($article_id).'/comments/'.strval($comment_id),'DELETE');
		}


		/**
		 *
		 *   Toggle spam mark on a comment
		 *   -----------------------------
		 *   @access public
		 *   @param int $article_id Article ID
		 *   @param int $comment_id Comment ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function toggleSpam( $article_id, $comment_id )
		{
			// Check
			if (empty($article_id)) throw new VoogParamException('Missing input data: $article_id.');
			if (empty($comment_id)) throw new VoogParamException('Missing input data: $comment_id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/articles/'.strval($article_id).'/comments/'.strval($comment_id).'/toggle_spam','PUT');
		}


		/**
		 *
		 *   Delete all comments marked as spam
		 *   -----------------------------------
		 *   @access public
		 *   @param int $article_id Article ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteSpam( $article_id )
		{
			// Check
			if (empty($article_id)) throw new VoogParamException('Missing input data: $article_id.');

			// Make request
			return $this->VoogPHP->makeAPIrequest('/articles/'.strval($article_id).'/comments/delete_spam','DELETE');
		}


	}


?>