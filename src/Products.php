<?php


	/**
	 *
	 *   Voog PHP API
	 *   Product functions
	 *   -----------------
	 *   @package VoogPHP
	 *   @author Codelab Solutions OÜ <codelab@codelab.ee>
	 *
	 */


	namespace Codelab\VoogPHP;


	class Products extends VoogPHPComponent
	{


		/**
		 *
		 *   Get products
		 *   ------------
		 *   @access public
		 *   @param string $languageCode Language code for top-level values
		 *   @param array|object $data Filter data
		 *   @param bool $includeVariants Include variants
		 *   @param bool $includeTranslations Include translations
		 *   @param bool $includeLocations Include locations
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return array
		 *
		 */

		public function getProducts( $languageCode=null, $data=null, $includeVariants=false, $includeTranslations=false, $includeLocations=false )
		{
			// Request
			$requestParam=array();
			if (!empty($data))
			{
				if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');
				foreach ($data as $k => $v) $requestParam[$k]=$v;
			}
			if (!empty($languageCode)) $requestParam['language_code']=$languageCode;
			$include=array();
			if ($includeVariants) $include[]='variants';
			if ($includeTranslations) $include[]='translations';
			if ($includeLocations) $include[]='locations';
			if (!empty($include))
			{
				$requestParam['include']=join(',',$include);
			}

			// Fetch list
			$voogResponse=$this->VoogPHP->makeAPIrequest('/ecommerce/v1/products?'.http_build_query($requestParam));

			// Build response array
			$productsArray=array();
			foreach ($voogResponse as $product)
			{
				$productsArray[strval($product->id)]=$product;
			}

			// Return
			return $productsArray;
		}


		/**
		 *
		 *   Get product data
		 *   ----------------
		 *   @access public
		 *   @param int $id Product ID
		 *   @param string $languageCode Language code for top-level values
		 *   @param bool $includeVariants Include variants
		 *   @param bool $includeTranslations Include translations
		 *   @param bool $includeLocations Include locations
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function getProduct( $id, $languageCode=null, $includeVariants=false, $includeTranslations=false, $includeLocations=false )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Request
			$requestParam=array();
			if ($languageCode) $requestParam['language_code']=$languageCode;
			$include=array();
			if ($includeVariants) $include[]='variants';
			if ($includeTranslations) $include[]='translations';
			if ($includeLocations) $include[]='locations';
			if (!empty($include))
			{
				$requestParam['include']=join(',',$include);
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/products/'.strval($id).'?'.http_build_query($requestParam));
		}


		/**
		 *
		 *   Add a new product
		 *   -----------------
		 *   @access public
		 *   @param array|object $data Product attributes
		 *   @param array|object $translations Translations
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function addProduct( $data, $translations=null )
		{
			// Check
			if (empty($data)) throw new VoogParamException('Missing input data: $data.');
			if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');

			// Create data
			$requestData=new \stdClass();
			foreach ($data as $k => $v)
			{
				$requestData->{$k}=$v;
			}

			// Translations
			if (!empty($translations))
			{
				if (!is_array($translations) && !is_object($translations)) throw new VoogParamException('Invalid input data: $translations not an array or an object.');
				$requestData->translations=new \stdClass();
				foreach ($translations as $k => $translation)
				{
					if (!is_array($translation) && !is_object($translation)) throw new VoogParamException('Invalid input data: $translations['.$k.'] not an array or an object.');
					$requestData->translations->{$k}=new \stdClass();
					foreach ($translation as $lang => $value)
					{
						$requestData->translations->{$k}->{$lang}=$value;
					}
				}
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/products','POST',$requestData);
		}


		/**
		 *
		 *   Update product data
		 *   -------------------
		 *   @access public
		 *   @param int $id Product ID
		 *   @param array|object $data Product attributes
		 *   @param array|object $translations Translations
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function updateProduct( $id, $data, $translations=null )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');
			if (empty($data)) throw new VoogParamException('Missing input data: $data.');
			if (!is_array($data) && !is_object($data)) throw new VoogParamException('Invalid input data: $data not an array or an object.');

			// Create data
			$requestData=new \stdClass();
			foreach ($data as $k => $v)
			{
				$requestData->{$k}=$v;
			}

			// Translations
			if (!empty($translations))
			{
				if (!is_array($translations) && !is_object($translations)) throw new VoogParamException('Invalid input data: $translations not an array or an object.');
				$requestData->translations=new \stdClass();
				foreach ($translations as $k => $translation)
				{
					if (!is_array($translation) && !is_object($translation)) throw new VoogParamException('Invalid input data: $translations['.$k.'] not an array or an object.');
					$requestData->translations->{$k}=new \stdClass();
					foreach ($translation as $lang => $value)
					{
						$requestData->translations->{$k}->{$lang}=$value;
					}
				}
			}

			// Make request
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/products/'.strval($id),'PUT',$requestData);
		}


		/**
		 *
		 *   Delete a product
		 *   ----------------
		 *   @access public
		 *   @param int $id Product ID
		 *   @throws VoogParamException
		 *   @throws VoogRequestException
		 *   @return object
		 *
		 */

		public function deleteProduct( $id )
		{
			// Check
			if (empty($id)) throw new VoogParamException('Missing input data: $id.');

			// Delete
			return $this->VoogPHP->makeAPIrequest('/ecommerce/v1/products/'.strval($id),'DELETE');
		}


	}


?>